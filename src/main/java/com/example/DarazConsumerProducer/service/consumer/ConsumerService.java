package com.example.DarazConsumerProducer.service.consumer;


import com.example.DarazConsumerProducer.handler.RecordHandler;
import com.example.DarazConsumerProducer.handler.model.RecordCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsumerService {

    @Autowired
    RecordHandler recordHandler;

    @KafkaListener(topics = "#{'${consumer.topic}'.split(',')}")
    public void receive(@Payload List<String> data,
                        @Headers MessageHeaders headers, Acknowledgment acknowledgment) {


        acknowledgment.acknowledge();

        List<RecordCustom> recordCustom = RecordCustom.getRecords(data,headers);

        System.out.println(recordCustom);
        System.out.println(recordCustom.size());

        recordHandler.handle(recordCustom);


//        recordHandler.handle(recordCustom);
    }






}
