package com.example.DarazConsumerProducer.service.producer;


import com.example.DarazConsumerProducer.HelperClasses.HelpingUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;

@Service
public class ProducerService {



    @Autowired
    private KafkaTemplate<String, LinkedHashMap<String,Object>> kafkaTemplate;


    public void send(LinkedHashMap<String,Object> data,String key,String topic){


        String json = HelpingUtilities.convertToJsonString(data);

        if(json!=null){
            Message<String> message = MessageBuilder
                    .withPayload(json)
                    .setHeader(KafkaHeaders.MESSAGE_KEY,key)
                    .setHeader(KafkaHeaders.TOPIC, topic)
                    .build();

            kafkaTemplate.send(message);

        }

    }


    public void send(List<LinkedHashMap<String,Object>> dataList, String key, String topic){


        for(LinkedHashMap<String,Object> data:dataList) {
            String json = HelpingUtilities.convertToJsonString(data);

            if (json != null) {
                Message<String> message = MessageBuilder
                        .withPayload(json)
                        .setHeader(KafkaHeaders.MESSAGE_KEY, key)
                        .setHeader(KafkaHeaders.TOPIC, topic)
                        .build();
                kafkaTemplate.send(message);
            }
        }


    }
}