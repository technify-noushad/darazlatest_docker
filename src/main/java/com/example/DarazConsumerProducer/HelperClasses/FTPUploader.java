package com.example.DarazConsumerProducer.HelperClasses;

/**
 * Created by bilal on 31/12/18.
 */

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

public class FTPUploader {
    FTPClient ftp = null;

    public FTPUploader(String host, String user, String pwd) throws Exception {
        ftp = new FTPClient();
        ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
        int reply;
        ftp.connect(host);
        reply = ftp.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            try {
                ftp.disconnect();
                throw new Exception("Exception in connecting to FTP Server");
            }
            catch (Exception e) {
                e.printStackTrace();
                System.out.println("DarazConnector =>" + HelpingUtilities.StackTraceToString(e));
            }
        }
        ftp.login(user, pwd);
        ftp.setFileType(FTP.BINARY_FILE_TYPE);
        ftp.enterLocalPassiveMode();
    }

    public boolean uploadFile(BufferedImage bufferedImage, String imageName, String hostDir)
            throws Exception {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "png", baos);
        InputStream is = new ByteArrayInputStream(baos.toByteArray());
        return this.ftp.storeFile(hostDir + imageName + ".png", is);
    }

    public void uploadFile(String imageName, String hostDir,String localdir) throws FileNotFoundException {
        FileInputStream fis = new FileInputStream(localdir);
        try {
            this.ftp.storeFile(hostDir + imageName+".csv", fis);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("DarazConnector =>" + HelpingUtilities.StackTraceToString(e));
        }

    }


    public void disconnect(){
        if (this.ftp.isConnected()) {
            try {
                this.ftp.logout();
                this.ftp.disconnect();
            } catch (IOException f) {
                // do nothing as file is already saved to server
            }
        }
    }
}