package com.example.DarazConsumerProducer.HelperClasses;

import com.example.DarazConsumerProducer.Constants.ProjectConstants;
import org.springframework.data.util.Pair;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;

//import javafx.util.Pair;

/**
 * Created by yousuf on 7/6/17.
 */
public class CustomImageHandler {
    private static int MAX_WIDTH = 800;
    private static int MAX_HEIGHT = 800;
    private static int MIN_WIDTH = 500;
    private static int MIN_HEIGHT = 500;
    static float MAX_RATIO = (float)MAX_HEIGHT/(float)MIN_WIDTH;
    static float MIN_RATIO = (float)MIN_HEIGHT/(float)MAX_WIDTH;

    public static BufferedImage resizeImage(BufferedImage originalImage, int type){
        int[] ImageDimension = CalculateDimensionToTransform(originalImage);
//        float ratio = (float) ImageDimension[1]/(float) ImageDimension[0];
        int actualHeight=ImageDimension[1];
        int actualWidth=ImageDimension[0];
        if(ImageDimension[0]<MIN_WIDTH){
            actualWidth = MIN_WIDTH;
        }
        if(ImageDimension[1]<MIN_HEIGHT)
        {
            actualHeight = MIN_HEIGHT;
        }
        BufferedImage resizedImage = new BufferedImage(actualWidth, actualHeight, type);


        Graphics2D g = resizedImage.createGraphics();
        g.setPaint (new Color(255,255,255));
        g.fillRect (0, 0, resizedImage.getWidth(), resizedImage.getHeight());
        g.drawImage(originalImage, (actualWidth-ImageDimension[0])/2, (actualHeight-ImageDimension[1])/2, ImageDimension[0], ImageDimension[1], null);
        g.dispose();

        return resizedImage;
    }
    private static int[] CalculateDimensionToTransform(BufferedImage image){
        int width = image.getWidth();
        int height = image.getHeight();
        float ratio = (float) height/(float) width;
        if(ratio < MIN_RATIO){
            if(width<MIN_WIDTH){
                width = MIN_WIDTH;
            }
            else if(width>MAX_WIDTH){
                width = MAX_WIDTH;
            }
            height = (int)(width*ratio);

        }
        else if(ratio > MAX_RATIO){

            if(height<MIN_HEIGHT){
                height = MIN_HEIGHT;
            }
            else if(height>MAX_HEIGHT){
                height = MAX_HEIGHT;
            }
            width = (int)(height/ratio);

        }
        if((width >= MIN_WIDTH && width <= MAX_WIDTH) && (height >= MIN_HEIGHT && height <= MAX_HEIGHT)){
            return new int[]{width,height};
        }
        else if(height > width){
            if(height>MAX_HEIGHT){
                height = MAX_HEIGHT;
            }
            width = (int)(height / ratio);

        }
        else if(height < width){
            if(width>MAX_WIDTH){
                width = MAX_WIDTH;
            }
            height = (int)(width*ratio);

        }
        else{
            if(width>MAX_WIDTH){
                width = MAX_WIDTH;
            }
            height = (int)(width*ratio);

        }
         if(ratio<=MAX_RATIO && ratio>=MIN_RATIO){

            if(height<MIN_HEIGHT){
                height = MIN_HEIGHT;
                width = (int)(height/ratio);

            }
            else if(height>MAX_HEIGHT){
                height = MAX_HEIGHT;
                width = (int)(height/ratio);

            }
            else if(width<MIN_WIDTH){
                width = MAX_WIDTH;
                height = (int)(width*ratio);

            }
            else if(width>MAX_WIDTH){
                width = MAX_WIDTH;
                height = (int)(width*ratio);
            }

        }
        return new int[]{width,height};
    }

    public static String ImageDownload(String urlString,boolean changeUrl, String name) throws IOException {
//        URL url = new URL(urlString);
//        BufferedImage image = ImageIO.read(url);
        return urlString;
//        return SaveImageToServer(image, name);
    }

    public static String ImageDownloader(String urlString,boolean changeUrl, String name){

        System.out.println("DarazConnector =>" + "DarazConnector => urlString->" + urlString);
        System.out.println("DarazConnector =>" + name);
        Pair<BufferedImage,String> pair =null;

        try {
            urlString =  urlString.replace(" ","%20").split("\\?")[0];
            URL url = new URL(urlString);

            URLConnection connection = url.openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");

            connection.connect();

            InputStream inputStream = connection.getInputStream();
            String type = null;
            BufferedImage image ;

            ImageInputStream imageInput = ImageIO.createImageInputStream(inputStream);
            Iterator<ImageReader> imageReaders = ImageIO.getImageReaders(imageInput);
            while (imageReaders.hasNext()) {
                ImageReader reader = imageReaders.next();
                type = reader.getFormatName();
                System.out.printf("formatName: %s%n", type);
            }

            if(type == null)
            {
                if(urlString.contains("https"))
                    url = new URL(urlString.replace("https", "http"));
                else
                    url = new URL(urlString.replace("http","https"));
                URLConnection connection1 = url.openConnection();
                connection1.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");

                connection1.connect();

                InputStream inputStream1 = connection1.getInputStream();

                ImageInputStream imageInput1 = ImageIO.createImageInputStream(inputStream1);
                Iterator<ImageReader> imageReaders1 = ImageIO.getImageReaders(imageInput1);
                while (imageReaders1.hasNext()) {
                    ImageReader reader = imageReaders1.next();
                    type = reader.getFormatName();
                    System.out.printf("formatName: %s%n", type);
                }
                image = ImageIO.read(imageInput1);

            }
            else {
                image = ImageIO.read(imageInput);
            }

            image = resizeImage(image, 1);

//            pair = new Pair<>(image,type);

            return SaveImageToServer(image, name);

        } catch (Exception e) {
            System.out.println("DarazConnector =>" + "DarazConnector => "+e.getMessage());
            e.printStackTrace();
            System.out.println("DarazConnector =>" + HelpingUtilities.StackTraceToString(e));
        }

        return "";
    }

    public static String SaveImageLocally(BufferedImage image,String name,String formatName){
        try {
            String ImagePath = ProjectConstants.FolderPath +name+"."+formatName;
            System.out.println("DarazConnector =>" + ImagePath);
            ImageIO.write(image, formatName, new File(ImagePath));
            return ImagePath;
        } catch (Exception e) {
            System.out.println("DarazConnector =>" + "DarazConnector => "+e.getMessage());
            e.printStackTrace();
            System.out.println("DarazConnector =>" + HelpingUtilities.StackTraceToString(e));
        }
       return "";
    }

    public static String SaveImageToServer(BufferedImage image,String imageName){
        FTPUploader ftpUploader = null;
        try {
            ftpUploader = new FTPUploader("35.200.244.36",
                    "daraz_ftp",
                    "user@technify");
            boolean uploadFile = ftpUploader.uploadFile(image, imageName,"/home/daraz/");
            ftpUploader.disconnect();
            if(uploadFile)
            {
                System.out.println("DarazConnector =>" + "Done");
                return "http://files.technify.tech/daraz/"+imageName+".png";
            }
            else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String downloadImage(String URLString , String name)
    {
        Image image = null;
        try {
            URL url = new URL(URLString);
            image = ImageIO.read(url.openStream());
            System.out.println("DarazConnector =>" + image);
            BufferedImage image1 = toBufferedImage(image);

            image1 = resizeImage(image1, 1);

            return SaveImageToServer(image1, name);
        } catch (IOException e) {
            System.out.println("DarazConnector =>" + e.getMessage());
        }
        return "";
    }

    public static BufferedImage toBufferedImage(Image img)
    {
        if (img instanceof BufferedImage)
        {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }
}