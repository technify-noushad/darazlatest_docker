package com.example.DarazConsumerProducer.HelperClasses;

import com.example.DarazConsumerProducer.Constants.ProjectConstants;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.JestResult;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.ClearScroll;
import io.searchbox.core.Search;
import io.searchbox.core.SearchScroll;
import io.searchbox.params.Parameters;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ElasticSearchCalls {

    public static int limit = 10;

    private String scrollId = "";
    JestClient client;

    public ElasticSearchCalls(String scrollId){

        setClient();
        this.scrollId = scrollId;
    }

    public ElasticSearchCalls(){
        setClient();

    }

    /*private void setClient() {
        JestClientFactory factory = new JestClientFactory();
        factory.setHttpClientConfig(new HttpClientConfig
                .Builder(ProjectConstants.elasticEndPoint)
                .multiThreaded(true)
                .defaultCredentials(ProjectConstants.elasticUserName, ProjectConstants.elasticPass)
                .build());
        client = factory.getObject();
    }*/

    private void setClient() {

        JestClientFactory factory = new JestClientFactory();

        List<String> stringList = Arrays.asList(ProjectConstants.elasticEndPoint.split(","));
        stringList = stringList.stream().map(i->"http://"+i+":"+ProjectConstants.elasticPort).collect(Collectors.toList());
        factory.setHttpClientConfig(new HttpClientConfig
                .Builder(stringList)
                .multiThreaded(true)
                .readTimeout(50000)
                .defaultCredentials(ProjectConstants.elasticUserName,ProjectConstants.elasticPass).build());

        client = factory.getObject();
    }


    public enum ElasticType{

        OPTION("option"),PRODUCT("product"),BRAND("brand");
        ElasticType(String value) {
            this.value = value;
        }
        private final String value;
        public String getValue() {
            return value;
        }

    }

    public enum ElasticIndex{

        OPTIONS("options"), REPLICATE_CATALOG("replicate_catalog"),BRANDS("brands");
        //        OPTION("otherinfo"), REPLICATE_CATALOG("replicate_catalog");
        ElasticIndex(String value) {
            this.value = value;
        }
        private final String value;
        public String getValue() {
            return value;
        }

    }
    public List<LinkedHashMap<String, Object>> fetchNext(int scrollWindowInMinutes) {
        return processScroll(this.scrollId, scrollWindowInMinutes);
    }

    public List<LinkedHashMap<String, Object>> fetchScroll(String scrollId,int scrollWindowInMinutes) {
        return processScroll(scrollId, scrollWindowInMinutes);
    }

    private List<LinkedHashMap<String, Object>> processScroll(String scrollId, int scrollWindowInMinutes) {

        if(scrollId!=null && !scrollId.equals("")) {

            SearchScroll scroll = new SearchScroll.Builder(scrollId, scrollWindowInMinutes+"m").build();
            try {
                JestResult result = client.execute(scroll);
                if(result.getResponseCode()==200) {
                    if(result.getJsonObject().has("_scroll_id")) {
                        this.scrollId = result.getJsonObject().get("_scroll_id").getAsString();
                    }
                    List<LinkedHashMap<String, Object>> listOfObjects = new ObjectMapper().readValue(result.getJsonObject().get("hits")
                            .getAsJsonObject().get("hits").getAsJsonArray().toString(),new TypeReference<List<LinkedHashMap<String, Object>>>(){});
                    for(int i=0;i<listOfObjects.size();i++){
                        listOfObjects.set(i,(LinkedHashMap<String, Object>)listOfObjects.get(i).get("_source"));
                    }
                    if(listOfObjects.size()<limit){
                        ClearScroll clearScroll = new ClearScroll.Builder().addScrollId(scrollId).build();
                        result = client.execute(clearScroll);
                        if(result.getResponseCode()==200){
                            System.out.println("DarazConnector =>" + "Successfully remove scroll_id:\t:"+scrollId);
                        }
                    }
                    return listOfObjects;
                }
                else return null;
// Henry@3366

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

        }
        return null;
    }

    public List<LinkedHashMap<String, Object>> fetchByQuery(ElasticIndex index,ElasticType type,String query,int scrollWindowInMinutes) {


        return fetchFromQuery(index, type, scrollWindowInMinutes, client, query);

    }

    public List<LinkedHashMap<String, Object>> fetchFromElasticBySKUs(ElasticIndex index,ElasticType type,String store_id, String sku, String variant_id,int scrollWindowInMinutes) {

        String query = "{ \"query\": { \"bool\": { \"must\": [ { \"term\": { \"store_id.keyword\": \""+ store_id +"\" } } , { \"term\": { \"sku.keyword\": \""+sku+"\" } } ], \"must_not\": [ ], \"should\": [ ] } }, \"from\": 0, \"size\": 1000, \"sort\": [ ], \"aggs\": { } }";
//        String query= "{ \"query\": { \"bool\": { \"must\": [ { \"term\": { \"store_id.keyword\": \""+store_id+"\" } } , { \"term\": { \"variants.id.keyword\": \""+variant_id+"\" } } , { \"term\": { \"variants.sku.keyword\": \""+sku+"\" } } ], \"must_not\": [ ], \"should\": [ ] } }, \"from\": 0, \"size\": 1000, \"sort\": [ ], \"aggs\": { } }";
        query = String.format(query,HelpingUtilities.convertToJsonString(""));

        return fetchFromQuery(index, type, scrollWindowInMinutes, client, query);

    }


    public List<LinkedHashMap<String, Object>> fetchFromElasticByIds(ElasticIndex index,ElasticType type,List<String> ids,int scrollWindowInMinutes) {

        String query = " {\"size\":"+ limit+",\"query\":{\"ids\":{\"type\":[],\"values\":%s,\"boost\":1}}}";
        query = String.format(query,HelpingUtilities.convertToJsonString(ids));

        return fetchFromQuery(index, type, scrollWindowInMinutes, client, query);

    }

    public List<LinkedHashMap<String, Object>> fetchFromElasticByStoreID(ElasticIndex index,ElasticType type,String storeID,int scrollWindowInMinutes) {

        String query = "{ \"query\": { \"bool\": { \"must\": [ { \"term\": { \"store_id.keyword\": \"" + storeID + "\" } } , { \"term\": { \"status\": \"1\" } } , { \"range\": { \"quantity\": { \"gt\": \"0\" } } } ], \"must_not\": [ ], \"should\": [ ] } }, \"from\": 0, \"size\": "+limit+", \"sort\": [ ], \"aggs\": { } }";

        query = String.format(query,HelpingUtilities.convertToJsonString(""));
        return fetchFromQuery(index, type, scrollWindowInMinutes, client, query);

    }

    public List<LinkedHashMap<String, Object>> fetchFromElasticByVariantsColor(ElasticIndex index,ElasticType type,String storeID,int scrollWindowInMinutes) {

        String query = "{\"query\":{\"bool\":{\"must\":[{\"term\":{\"variants.option_values.option_name.keyword\":\"color\"}}],\"must_not\":[],\"should\":[]}},\"from\":0,\"size\":3,\"sort\":[],\"aggs\":{}}";
        query = String.format(query,HelpingUtilities.convertToJsonString(""));
        return fetchFromQuery(index, type, scrollWindowInMinutes, client, query);

    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if(client!=null) {
            client.close();
        }
    }

    private List<LinkedHashMap<String, Object>> fetchFromQuery(ElasticIndex index, ElasticType type, int scrollWindowInMinutes, JestClient client, String query) {
        Search.Builder builder = new Search.Builder(query);

        builder.addIndex(index.getValue())
                .addType(type.getValue());
        if(scrollWindowInMinutes!=0){
            builder.setParameter(Parameters.SCROLL, scrollWindowInMinutes+"m");
        }
        Search search = builder.build();
        try {
            JestResult result = client.execute(search);
            if(result.getResponseCode()==200) {

                if(result.getJsonObject().has("_scroll_id")) {
                    this.scrollId = result.getJsonObject().get("_scroll_id").getAsString();
                }

                String json = result.getJsonObject().get("hits")
                        .getAsJsonObject().get("hits").getAsJsonArray().toString();
                List<LinkedHashMap<String, Object>> listOfObjects = new ObjectMapper().readValue(json,new TypeReference<List<LinkedHashMap<String, Object>>>(){});

                for(int i=0;i<listOfObjects.size();i++){
                    listOfObjects.set(i,(LinkedHashMap<String, Object>)listOfObjects.get(i).get("_source"));
                }
                return listOfObjects;
            }
            else
                return null;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }


//    public List<LinkedHashMap<String, Object>> fetchFromElasticByQuery(ElasticIndex index,ElasticType type,Map<String,Object> query,int scrollWindowInMinutes) {
//        JestClientFactory factory = new JestClientFactory();
//        factory.setHttpClientConfig(new HttpClientConfig
//                .Builder(ProjectConstants.elasticEndPoint)
//                .multiThreaded(true)
//                .build());
//        JestClient client = factory.getObject();
//
//
//
//        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
//
//        for(String key:query.keySet()){
//            boolQueryBuilder.filter(QueryBuilders.termQuery(key,query.get(key)));
//        }
//
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(boolQueryBuilder);
//        return fetchFromQuery(index, type, scrollWindowInMinutes, client, searchSourceBuilder);
//
//    }
}
