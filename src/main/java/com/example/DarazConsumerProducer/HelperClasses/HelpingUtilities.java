package com.example.DarazConsumerProducer.HelperClasses;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

public class HelpingUtilities {
    private static ObjectMapper objectMapper = new ObjectMapper()
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    public static <T> T convertToObject(String string, TypeReference<T> tClass){

        try {
            return objectMapper.readValue(string,tClass);
        } catch (IOException e) {
            //TODO: report to developer
            e.printStackTrace();
        }
        return null;

    }

    public static String convertToJsonString(Object object){
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            //TODO: report to develop
            e.printStackTrace();
        }
        return null;
    }
    public static ObjectNode getObjectNode(Object object){

        return objectMapper.valueToTree(object);
    }


    public static byte[] getBytes(Object object){

        try {
            return objectMapper.writeValueAsBytes(object);
        } catch (JsonProcessingException e) {
            //TODO: report to develop
            e.printStackTrace();
        }
        return null;
    }

    public static String StackTraceToString(Exception ex) {
        StringBuilder result = new StringBuilder(ex.toString() + "\n");
        StackTraceElement[] trace = ex.getStackTrace();
        for (StackTraceElement aTrace : trace) {
            result.append(aTrace.toString()).append("\n");
        }
        return result.toString();
    }

}
