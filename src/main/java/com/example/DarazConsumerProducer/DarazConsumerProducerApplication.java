package com.example.DarazConsumerProducer;

import com.example.DarazConsumerProducer.Main.MainApplicationClass;
import com.example.DarazConsumerProducer.handler.RecordHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@SpringBootApplication
@EnableScheduling
public class DarazConsumerProducerApplication implements CommandLineRunner {

	public static void main(String[] args) {
		Properties properties = null;
		try (InputStream inputStream = new FileInputStream(args[0])) {
			properties = new Properties();
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}

		if(properties==null)
			return;
		new SpringApplicationBuilder(DarazConsumerProducerApplication.class)
				.properties(properties).web(WebApplicationType.NONE).run(args);
	}




	@Autowired
	private
	RecordHandler recordHandler;


	@Override
	public void run(String... strings) throws Exception {



		System.out.println("Module Started");

	}

}
