package com.example.DarazConsumerProducer.DarazSDK;

import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class DarazAPIClass {

    public String UpdatePriceQuantity(String UserID, String APIKey, String XMLBody)
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserID", UserID);
        params.put("Timestamp", getCurrentTimestamp());
        params.put("Version", "1.0");
        params.put("Format", "JSON");
        params.put("Action", "UpdatePriceQuantity");
        final String apiKey = APIKey;
        final String XML = XMLBody;
        final String out = new SellercenterAPI().getSellercenterApiResponse(params, apiKey, XML); // provide XML as an empty string when not needed
        System.out.println("DarazConnector =>" + out); // print out the XML response

        return out;
    }

    public String GetCategoryTree(String UserID, String APIKey, String XMLBody)
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserID", UserID);
        params.put("Timestamp", getCurrentTimestamp());
        params.put("Version", "1.0");
        params.put("Format", "JSON");
        params.put("Action", "GetCategoryTree");
        final String apiKey = APIKey;
        final String XML = XMLBody;
        final String out = new SellercenterAPI().getSellercenterApiResponse(params, apiKey, XML); // provide XML as an empty string when not needed
        System.out.println("DarazConnector =>" + out); // print out the XML response

        return out;
    }


    public String GetCategoryAttributes(String UserID, String APIKey, String XMLBody, String categoryID)
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserID", UserID);
        params.put("Timestamp", getCurrentTimestamp());
        params.put("Version", "1.0");
        params.put("Format", "JSON");
        params.put("Action", "GetCategoryAttributes");
        params.put("PrimaryCategory", categoryID);
        final String apiKey = APIKey;
        final String XML = XMLBody;
        final String out = new SellercenterAPI().getSellercenterApiResponse(params, apiKey, XML); // provide XML as an empty string when not needed
        //System.out.println("DarazConnector =>" + out); // print out the XML response
        return out;
    }

    public String MigrateImage(String UserID, String APIKey, String XMLBody)
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserID", UserID);
        params.put("Timestamp", getCurrentTimestamp());
        params.put("Version", "1.0");
        params.put("Format", "JSON");
        params.put("Action", "MigrateImage");
//        params.put("image", XMLBody);
        final String apiKey = APIKey;
        final String XML = XMLBody;
        String out = new SellercenterAPI().getSellercenterApiResponse(params, apiKey, XML); // provide XML as an empty string when not needed
        System.out.println("DarazConnector =>" + out); // print out the XML response

        JSONObject jsonObject = new JSONObject(out);
        if(jsonObject.has("SuccessResponse"))
        {
            out = jsonObject.getJSONObject("SuccessResponse").getJSONObject("Body").getJSONObject("Image").get("Url").toString();
        }
        else {
            out = "";
        }
        return out;
    }

    public String SetImages(String UserID, String APIKey, String XMLBody)
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("UserID", UserID);
        params.put("Timestamp", getCurrentTimestamp());
        params.put("Version", "1.0");
        params.put("Format", "JSON");
        params.put("Action", "SetImages");
        final String apiKey = APIKey;
        final String XML = XMLBody;
        String out = new SellercenterAPI().getSellercenterApiResponse(params, apiKey, XML); // provide XML as an empty string when not needed
        System.out.println("DarazConnector =>" + out); // print out the XML response

        JSONObject jsonObject = new JSONObject(out);
        if(jsonObject.has("SuccessResponse"))
        {
            return out;
        }
        else {
            out = "";
        }
        return out;
    }

    public String CreateProduct(String UserID, String APIKey, String XMLBody)
    {
        XMLBody = XMLBody.replace(" & ", "and");

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserID", UserID);
        params.put("Timestamp", getCurrentTimestamp());
        params.put("Version", "1.0");
        params.put("Format", "JSON");
        params.put("Action", "CreateProduct");
        final String apiKey = APIKey;
        final String XML = XMLBody;
        final String out = new SellercenterAPI().getSellercenterApiResponse(params, apiKey, XML); // provide XML as an empty string when not needed
        System.out.println("DarazConnector =>" + out); // print out the XML response

        return out;
    }

    public String CreateProduct1(String UserID, String APIKey, String XMLBody) throws IOException {
        XMLBody = XMLBody.replace(" & ", "and");

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserID", UserID);
        params.put("Timestamp", getCurrentTimestamp());
        params.put("Version", "1.0");
        params.put("Format", "JSON");
        params.put("Action", "CreateProduct");
        final String apiKey = APIKey;
        final String XML = XMLBody;
        final String out = new SellercenterAPI().getAPIResponse(params, apiKey, XML); // provide XML as an empty string when not needed
        System.out.println("DarazConnector =>" + out); // print out the XML response

        return out;
    }

    public String UpdateProduct(String UserID, String APIKey, String XMLBody)
    {
        XMLBody = XMLBody.replace(" & ", "and");

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserID", UserID);
        params.put("Timestamp", getCurrentTimestamp());
        params.put("Version", "1.0");
        params.put("Format", "JSON");
        params.put("Action", "UpdateProduct");
        final String apiKey = APIKey;
        final String XML = XMLBody;
        final String out = new SellercenterAPI().getSellercenterApiResponse(params, apiKey, XML); // provide XML as an empty string when not needed
        System.out.println("DarazConnector =>" + out); // print out the XML response

        return out;
    }
    /**
     * returns the current timestamp
     * @return current timestamp in ISO 8601 format
     */
    public static String getCurrentTimestamp(){
        final TimeZone tz = TimeZone.getTimeZone("UTC");
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
        df.setTimeZone(tz);
        final String nowAsISO = df.format(new Date());
        return nowAsISO;
    }
}
