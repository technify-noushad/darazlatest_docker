package com.example.DarazConsumerProducer.handler;


import com.example.DarazConsumerProducer.Constants.ProjectConstants;
import com.example.DarazConsumerProducer.Logs.L;
import com.example.DarazConsumerProducer.Main.MainApplicationClass;
import com.example.DarazConsumerProducer.Main.UpdateProductsClass;
import com.example.DarazConsumerProducer.Main.WorkingClass;
import com.example.DarazConsumerProducer.component.ContextProvider;
import com.example.DarazConsumerProducer.handler.model.RecordCustom;
import com.example.DarazConsumerProducer.service.producer.ProducerService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Callable;

public class DarazRecordHandlerCallable implements Callable {


    static String className = "DarazRecordHandlerCallable";
    private List<RecordCustom> recordCustoms;

    private static final Gson gson = new Gson();

    private Map<String,Map<String,Boolean>> topicMap;

    private String column;
    private String jsonStringFields;


    private ProducerService producerService = ContextProvider.getBean(ProducerService.class);


    public DarazRecordHandlerCallable(List<RecordCustom> recordCustoms,
                                      Map<String, Map<String, Boolean>> topicMap
                                            ) {

        this.recordCustoms=recordCustoms;
        this.topicMap = topicMap;
    }

    @Override
    public Object call() throws Exception {

        handleRecords();


        return null;
    }


    private static String generateTimeStamp() {

        TimeZone timeZone =  TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timeZone);
        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS", Locale.US);
        simpleDateFormat.setTimeZone(timeZone);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static Date generateStandardDateTimestamp() {

        TimeZone timeZone =  TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timeZone);
        return calendar.getTime();
    }



    private void handleRecords() throws IOException
    {
        for(RecordCustom eachRecord :recordCustoms)
        {
            /* System.out.println("listSinkRecord : " + listSinkRecords); */

            L.s(ProjectConstants.devName, ProjectConstants.projectName, className, "SinkRecord ", "DarazConnector-> \n\n**************NEW RECORD***********", "N/A");

            L.s(ProjectConstants.devName, ProjectConstants.projectName, className, "SinkRecord => Record -> ", eachRecord.toString(), "N/A");

            L.s(ProjectConstants.devName, ProjectConstants.projectName, className, "SinkRecord => record.value->", eachRecord.getPayloadString(), "N/A");


            System.out.println("DarazConnector-> \n\n**************NEW RECORD***********");

            System.out.println("DarazConnector-> Key " + eachRecord.getKey());

            HashMap<String,Object> recordMap = (HashMap<String,Object>) eachRecord.getPayload();
            System.out.println("DarazConnector-> Record : " + recordMap);

            MainApplicationClass mainApplicationClass = new MainApplicationClass();
            try {

                L.s(ProjectConstants.devName, ProjectConstants.projectName, className, "SinkRecord => recordMap -> ", recordMap.toString(), "N/A");
                System.out.println(recordMap);

                //System.out.println("ShippingConnector-> record JSON : " + new Gson().toJson(recordMap));

                L.s(ProjectConstants.devName, ProjectConstants.projectName, className, "SinkRecord => ", "DarazConnector", "N/A");
                //System.out.println();

                if(recordMap != null) {

                    String valueMap = new Gson().toJson(recordMap);
                    LinkedHashMap<String, Object> value = new Gson().fromJson(valueMap, new TypeToken<LinkedHashMap<String, Object>>() {}.getType());


                    L.s(ProjectConstants.devName, ProjectConstants.projectName, className, "Reading Data from Topic DarazConnector", eachRecord.getPayloadString(), "N/A");

                    System.out.println("DarazConnector-> valueMap : " + value);

                    if (recordMap.get("action") != null) {
                        String action = recordMap.get("action").toString();
                        System.out.println("DarazConnector-> action : " + action);
                        if (action.toLowerCase().equals("migration")) {
                            System.out.println("DarazConnector => Migration");
                            System.out.println(new Gson().toJson(recordMap));
                            mainApplicationClass.connectorMainFunction(recordMap.get("store_id").toString(), "new_migration");
                        } else if (action.toLowerCase().equals("pushupdate")) {
                            new WorkingClass().pushUpdates(value);
                        }
                        else if (action.toLowerCase().equals("update_migration")) {
                            mainApplicationClass.connectorMainFunction(recordMap.get("store_id").toString(), "update_migration");
                        }
                    }
                }
            }catch (Exception e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
                L.e(ProjectConstants.devName, ProjectConstants.projectName, className, "Thread Callable",e.getStackTrace().toString(),"N/A");
            }
        }

    }

}


