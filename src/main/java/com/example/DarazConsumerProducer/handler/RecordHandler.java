package com.example.DarazConsumerProducer.handler;


import com.example.DarazConsumerProducer.Main.MainApplicationClass;
import com.example.DarazConsumerProducer.handler.model.RecordCustom;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Service
public class RecordHandler {

    private ExecutorService broadCasterExecutorService = Executors.newFixedThreadPool(100);
    private Map<String,Map<String,Boolean>> topicMap;

    public void handle(List<RecordCustom> recordCustomList) {
        DarazRecordHandlerCallable darazRecordHandlerCallable = new DarazRecordHandlerCallable(recordCustomList, topicMap);
        broadCasterExecutorService.submit(darazRecordHandlerCallable);
    }


    @Scheduled(fixedDelay = 180000)
    public void schedularForErrorProductPushUpdates()
    {
        new MainApplicationClass().schedularForErrorResponsePushUpdates();
    }



//    public void handle(RecordCustom recordCustom) {
//
//
//        if(recordCustom.getPayload()!=null){
//            LinkedHashMap<String,Object> linkedHashMap = recordCustom.getPayload();
//            String action = null;
//            if(recordCustom.getPayload().containsKey("action"))
//                action=recordCustom.getPayload().get("action").toString();
//
//            if(action==null)
//                return;
//            if(action.toLowerCase().equals("update")){
//                if(!linkedHashMap.containsKey("spec_id") ||
//                !linkedHashMap.containsKey("source")||
//                        !linkedHashMap.containsKey("dest") ||
//                        !linkedHashMap.containsKey("status")){
//                    return;
//                }
//
//
//                boolean status = update(linkedHashMap.get("spec_id").toString(),
//                        linkedHashMap.get("source").toString(),
//                        linkedHashMap.get("dest").toString(),
//                        (boolean)linkedHashMap.get("status"));
//
//                if(!status)
//                    technifyLogger.createAndSendLog(true,"Cannot update");
//
//            }
//            else if(action.toLowerCase().equals("loadconfig")){
//                loadConfig();
//            }
//        }
//
//    }

    @PreDestroy
    public void destroy() {
        broadCasterExecutorService.shutdownNow();
        System.out.println("Record Handler destroy");
    }


//    private boolean update(String specid, String source, String dest, boolean status){
//
//
//        String spec = SpecUtils.getSpecFromNifi(specid);
//
//        LinkedHashMap<String,LinkedHashMap<String,Boolean>> broadCasterMap = HelpingUtilities.convertToObject(spec,
//                new TypeReference<LinkedHashMap<String, LinkedHashMap<String, Boolean>>>() {});
//
//        if(broadCasterMap==null) {
//            //TODO report error
//            return false;
//        }
//        if(!broadCasterMap.containsKey(source)){
//            broadCasterMap.put(source,new LinkedHashMap<>());
//
//        }
//        broadCasterMap.get(source).put(dest,status);
//
//
//        int responseCode = SpecUtils.createSpecs(specid,HelpingUtilities.convertToJsonString(broadCasterMap));
//
//        if(responseCode==200){
//
//
//            loadConfig();
//
//        }
//        return responseCode==200;
//
//    }

//    public void loadConfig() {
//
//
//        String spec = SpecUtils.getSpecFromNifi(broadcasterConfig);
//
//        if(isWebHook){
//            this.webServiceProxy = ServiceGenerator.getWebServiceProxyGson("http://fake.pk");
//        }
//
//
//        if (spec!=null && !spec.equals("")) {
//            try {
//                topicMap = new ObjectMapper().readValue(spec,new TypeReference<Map<String, Map<String,Boolean>>>(){});
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        else{
//            throw new NullPointerException("BroadCaster v4 ERROR Spec is not right");
//        }
//
//    }






}
