package com.example.DarazConsumerProducer.handler.model;


import com.example.DarazConsumerProducer.HelperClasses.HelpingUtilities;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.messaging.MessageHeaders;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class RecordCustom {

    private String topicName;
    private long offset;
    private long timeStamp;
    private String key;
    private LinkedHashMap<String,Object> payload;
    private String payloadString;



    public RecordCustom(String data, MessageHeaders headers) {
        this.payload = HelpingUtilities
                .convertToObject(data,
                        new TypeReference<LinkedHashMap<String, Object>>() {});
        if(headers.get("kafka_receivedMessageKey")!=null && ((List<String>)headers.get("kafka_receivedMessageKey")).size()>0)
            this.key = ((List<String>)headers.get("kafka_receivedMessageKey")).get(0);
        this.offset = ((List<Long>)headers.get("kafka_offset")).get(0);
        this.timeStamp = ((List<Long>)headers.get("kafka_receivedTimestamp")).get(0);
        this.topicName = ((List<String>)headers.get("kafka_receivedTopic")).get(0);
        this.payloadString =data;
    }


    public RecordCustom(String topicName, long offset, long timeStamp, String key, LinkedHashMap<String, Object> payload, String payloadString) {
        this.topicName = topicName;
        this.offset = offset;
        this.timeStamp = timeStamp;
        this.key = key;
        this.payload = payload;
        this.payloadString = payloadString;
    }

    public static List<RecordCustom> getRecords(List<String> data, MessageHeaders headers) {

        List<RecordCustom> recordCustoms = new ArrayList<>();

        List<String> keys = null;
        if(headers.get("kafka_receivedMessageKey")!=null)
            keys = (List<String>)headers.getOrDefault("kafka_receivedMessageKey",null);
        List<Long> offsets = (List<Long>) headers.get("kafka_offset");
        List<Long> timeStamps = (List<Long>)headers.get("kafka_receivedTimestamp");
        List<String> topicName = (List<String> )headers.getOrDefault("kafka_receivedTopic",null);
        for(int i = 0;i<data.size();i++){


            recordCustoms.add(new RecordCustom(
                    topicName!=null?topicName.get(i):"",
                    offsets!=null?offsets.get(i):0,
                    timeStamps!=null?timeStamps.get(i):0,
                    keys!=null?keys.get(i):"",
                    HelpingUtilities.convertToObject(data.get(i), new TypeReference<LinkedHashMap<String,Object>>() {}),
                    data.get(i)
            ));




        }
        return recordCustoms;
    }

    public String getPayloadString() {
        return payloadString;
    }

    public void setPayloadString(String payloadString) {
        this.payloadString = payloadString;
    }

    public String getTopicName() {
        return topicName;
    }

    public long getOffset() {
        return offset;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getKey() {
        return key;
    }

    public LinkedHashMap<String, Object> getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        return "RecordCustom{" +
                "topicName='" + topicName + '\'' +
                ", offset=" + offset +
                ", timeStamp=" + timeStamp +
                ", key='" + key + '\'' +
                ", payload=" + payload +
                ", payloadString='" + payloadString + '\'' +
                '}';
    }
}
