package com.example.DarazConsumerProducer.cassandra;

import com.datastax.driver.core.Statement;
import com.datastax.driver.core.querybuilder.Clause;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraOperations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MyCassandraTemplate {

    static String className = "MyCassandraTemplate";

    @Autowired
    private CassandraOperations cassandraTemplate;

    public <T> T create(T entity) {
        try {
            cassandraTemplate.insert(entity);
            return entity;
        }
        catch (Exception e){
            return null;
        }
    }


    public <T> List<T> createBatch(List<T> entityList) {
        try{
            cassandraTemplate.batchOps().insert(entityList).execute();
            return entityList;
        }
        catch (Exception e){
            return null;
        }
    }




    public  <T> T update(T entity) {
        try{
            cassandraTemplate.update(entity);
            return entity;
        }
        catch (Exception e){
            return null;
        }
    }

    public <T> List<T> updateBatch(List<T> entityList) {
        try{

            cassandraTemplate.batchOps().update(entityList).execute();
            return entityList;
        }
        catch (Exception e){
            return null;
        }
    }


    public <T> T findById(Object id, Class<T> claz) {
        try{
            return cassandraTemplate.selectOneById(id,claz);
        }
        catch (Exception e){
            return null;
        }
    }

    public boolean deleteById(Object id, Class claz) {
        try{
            return  cassandraTemplate.deleteById(id, claz);

        }
        catch (Exception e){
            return false;
        }

    }

    public void delete(Object entity) {
        try{
            cassandraTemplate.delete(entity);
        }
        catch (Exception e){
        }

    }

    public <T> List<T>  deleteBatch(List<T> entities) {
        try{
            cassandraTemplate.batchOps().delete(entities).execute();
            return entities;
        }
        catch (Exception e){
            return null;
        }

    }


    public <T> List<T> getAll(String tableName,int limit,Class<T> claz) {
        Statement statement = QueryBuilder.select().all().from(tableName).limit(limit);
        return cassandraTemplate.select(statement,claz);
    }


    public <T> List<T> findAllBy(HashMap<String,Object> columnsToSearch,String tableName,int limit,Class<T> claz) {

        try{
            List<Clause> whereClauses = new ArrayList<Clause>();
            for(String key: columnsToSearch.keySet()){
                whereClauses.add(QueryBuilder.in(key,columnsToSearch.get(key)));
            }



            Select select;
            if(limit<1) {
                select = QueryBuilder.select().all().from(tableName);
            }
            else{
                select = QueryBuilder.select().all().from(tableName).limit(limit);
            }
            if(!whereClauses.isEmpty()) {

                Select.Where selectWhere = select.where();

                for (Clause clause : whereClauses) {

                    selectWhere.and(clause);

                }

                return cassandraTemplate.select(selectWhere.allowFiltering(),claz);


            }

            return null;

        }

        catch (Exception e){



            return null;

        }

    }



    public <T> List<T> getAll(String tableName,Class<T> claz) {
        Statement statement = QueryBuilder.select().all().from(tableName);
        return cassandraTemplate.select(statement,claz);
    }

//    public <T> List<T> findAllBy(HashMap<String,Object> columnsToSearch, String tableName, Class<T> claz) {
//        try{
//            List<Clause> whereClauses = new ArrayList<>();
//            for(String key: columnsToSearch.keySet()){
//                whereClauses.add(QueryBuilder.in(key,columnsToSearch.get(key)));
//            }
//
//            Select select = QueryBuilder.select().all().from(tableName);
//
//            if(!whereClauses.isEmpty()) {
//
//                Select.Where selectWhere = select.where();
//
//                for (Clause clause : whereClauses) {
//
//                    selectWhere.and(clause);
//
//                }
//
//                return cassandraTemplate.select(selectWhere.allowFiltering(),claz);
//
//            }
//
//            return null;
//        }
//        catch (Exception e){
//            L.e(Constants.devName, Constants.projectName, className, "findAllBy", e.getMessage());
//            return null;
//        }
//
//    }

    public <T> T findOneBy(HashMap<String,Object> columnsToSearch, String tableName, Class<T> claz) {
        try{
            List<Clause> whereClauses = new ArrayList<Clause>();
            for(String key: columnsToSearch.keySet()){
                whereClauses.add(QueryBuilder.eq(key,columnsToSearch.get(key)));
            }
            Select select = QueryBuilder.select().all().from(tableName);
            if(!whereClauses.isEmpty()) {
                Select.Where selectWhere = select.where();
                for (Clause clause : whereClauses) {
                    selectWhere.and(clause);
                }
                return cassandraTemplate.selectOne(selectWhere.allowFiltering(),claz);
            }
            return null;

        }
        catch (Exception e){

            return null;
        }

    }

/*    public <T> T findOneBy(HashMap<String,Object> columnsToSearch, String tableName, Class<T> claz) {
        try{
            List<Clause> whereClauses = new ArrayList<>();
            for(String key: columnsToSearch.keySet()){
                whereClauses.add(QueryBuilder.eq(key,columnsToSearch.get(key)));
            }
            Select select = QueryBuilder.select().all().from(tableName);
            if(!whereClauses.isEmpty()) {
                Select.Where selectWhere = select.where();
                for (Clause clause : whereClauses) {
                    selectWhere.and(clause);
                }
                T asd = cassandraTemplate.selectOne(selectWhere.allowFiltering(), );
                System.out.println("asdas");
            }
            return null;

        }
        catch (Exception e){
            L.e(Constants.devName, Constants.projectName, className, "findOneBy", e.getMessage());

            return null;
        }

    }*/


    public <T> void truncate(Class<T> claz) {
        try{
            cassandraTemplate.truncate(claz);
        }
        catch (Exception e){

        }
    }



    public <T> boolean exists(Object id, Class<T> claz) {
            return cassandraTemplate.exists(id, claz);
    }


    public <T> T ContainsBy(HashMap<String,Object> columnsToSearchForContain,HashMap<String,Object> columnsToSearchForValue, String tableName, Class<T> claz) {
        try{
            List<Clause> whereClauses = new ArrayList<Clause>();
            for(String key: columnsToSearchForContain.keySet()){
                whereClauses.add(QueryBuilder.contains(key,columnsToSearchForContain.get(key)));
            }

            for (String key : columnsToSearchForValue.keySet() ) {
                whereClauses.add(QueryBuilder.eq(key,columnsToSearchForValue.get(key)));
            }

            Select select = QueryBuilder.select().all().from(tableName);
            if(!whereClauses.isEmpty()) {
                Select.Where selectWhere = select.where();
                for (Clause clause : whereClauses) {
                    selectWhere.and(clause);
                }
                return cassandraTemplate.selectOne(selectWhere.allowFiltering(),claz);
            }
            return null;

        }
        catch (Exception e){
            return null;
        }

    }
}
