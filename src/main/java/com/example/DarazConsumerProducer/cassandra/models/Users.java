package com.example.DarazConsumerProducer.cassandra.models;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.Date;

@Table(value = "userauthtable")
public class Users {

    @PrimaryKey
    @Column(value="store_id")
    private String store_id;

    @Column(value="api_key")
    private String api_key;

    @Column(value="user_id")
    private String user_id;

    @Column(value="password")
    private String password;

    @Column(value="updated_at")
    private Date updated_at;

    public Users(String store_id, String api_key, String user_id, String password, Date updated_at) {
        this.store_id = store_id;
        this.api_key = api_key;
        this.user_id = user_id;
        this.password = password;
        this.updated_at = updated_at;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getApi_key() {
        return api_key;
    }

    @Override
    public String toString() {
        return "Users{" +
                "store_id='" + store_id + '\'' +
                ", api_key='" + api_key + '\'' +
                ", user_id='" + user_id + '\'' +
                ", password='" + password + '\'' +
                ", updated_at=" + updated_at +
                '}';
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }


    public static String getTableName(){
        return  "userauthtable";
    }
}
