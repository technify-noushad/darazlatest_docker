package com.example.DarazConsumerProducer.cassandra.models;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.Date;

@Table(value = "darazpushupdates_initial_logs")
public class DarazPushUpdates_Initial_Logs {
    @PrimaryKey
    @Column(value="store_id")
    private String store_id;

    @Column(value="daraz_sku_id")
    private String daraz_sku_id;

    @Column(value="request_xml")
    private String request_xml;

    @Column(value="response")
    private String response;

    @Column(value="updated_at")
    private Date updated_at;

    @Column(value="variant_id")
    private String variant_id;

    @Column(value="technify_id")
    private String technify_id;

    @Column(value="status")
    private String status;


    public DarazPushUpdates_Initial_Logs(String store_id, String daraz_sku_id, String request_xml, String response, Date updated_at, String variant_id, String technify_id, String status) {
        this.store_id = store_id;
        this.daraz_sku_id = daraz_sku_id;
        this.request_xml = request_xml;
        this.response = response;
        this.updated_at = updated_at;
        this.variant_id = variant_id;
        this.technify_id = technify_id;
        this.status = status;

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getDaraz_sku_id() {
        return daraz_sku_id;
    }

    public void setDaraz_sku_id(String daraz_sku_id) {
        this.daraz_sku_id = daraz_sku_id;
    }

    public String getRequest_xml() {
        return request_xml;
    }

    public void setRequest_xml(String request_xml) {
        this.request_xml = request_xml;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public String getVariant_id() {
        return variant_id;
    }

    public void setVariant_id(String variant_id) {
        this.variant_id = variant_id;
    }

    public String getTechnify_id() {
        return technify_id;
    }

    public void setTechnify_id(String technify_id) {
        this.technify_id = technify_id;
    }



    public static String getTableName(){
        return "darazpushupdates_initial_logs";
    }
}
