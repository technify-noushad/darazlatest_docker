package com.example.DarazConsumerProducer.cassandra.models;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.Date;
import java.util.List;

@Table(value = "darazproducts")
public class DarazProducts
{
    @PrimaryKey
    @Column(value="daraz_sku_id")
    private String daraz_sku_id;

    @Column(value="store_id")
    private String store_id;

    @Column(value="primary_cat_id")
    private String primary_cat_id;

    @Column(value="sku")
    private String sku;

    @Column(value="attributes")
    private String attributes;

    @Column(value="brand")
    private String brand;

    @Column(value="updated_at")
    private Date updated_at;

    @Column(value="mandatory_att")
    private List<String> mandatory_att;

    @Column(value="mandatory_sku")
    private List<String> mandatory_sku;

    @Column(value="create_status")
    private String create_status;

    @Column(value="variant_id")
    private String variant_id;

    @Column(value="technify_id")
    private String technify_id;

    @Column(value="request_xml")
    private String request_xml;

    @Column(value="response")
    private String response;

    @Column(value="is_active")
    private String is_active;


    public DarazProducts(String daraz_sku_id, String store_id, String primary_cat_id, String sku, String attributes, String brand, Date updated_at, List<String> mandatory_att, List<String> mandatory_sku, String create_status, String variant_id, String technify_id, String request_xml, String response, String is_active) {
        this.daraz_sku_id = daraz_sku_id;
        this.store_id = store_id;
        this.primary_cat_id = primary_cat_id;
        this.sku = sku;
        this.attributes = attributes;
        this.brand = brand;
        this.updated_at = updated_at;
        this.mandatory_att = mandatory_att;
        this.mandatory_sku = mandatory_sku;
        this.create_status = create_status;
        this.variant_id = variant_id;
        this.technify_id = technify_id;
        this.request_xml = request_xml;
        this.response = response;
        this.is_active = is_active;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getDaraz_sku_id() {
        return daraz_sku_id;
    }

    public void setDaraz_sku_id(String daraz_sku_id) {
        this.daraz_sku_id = daraz_sku_id;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getPrimary_cat_id() {
        return primary_cat_id;
    }

    public void setPrimary_cat_id(String primary_cat_id) {
        this.primary_cat_id = primary_cat_id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public List<String> getMandatory_att() {
        return mandatory_att;
    }

    public void setMandatory_att(List<String> mandatory_att) {
        this.mandatory_att = mandatory_att;
    }

    public List<String> getMandatory_sku() {
        return mandatory_sku;
    }

    public void setMandatory_sku(List<String> mandatory_sku) {
        this.mandatory_sku = mandatory_sku;
    }

    public String getCreate_status() {
        return create_status;
    }

    public void setCreate_status(String create_status) {
        this.create_status = create_status;
    }

    public String getVariant_id() {
        return variant_id;
    }

    public void setVariant_id(String variant_id) {
        this.variant_id = variant_id;
    }

    public String getTechnify_id() {
        return technify_id;
    }

    public void setTechnify_id(String technify_id) {
        this.technify_id = technify_id;
    }

    public String getRequest_xml() {
        return request_xml;
    }

    public void setRequest_xml(String request_xml) {
        this.request_xml = request_xml;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "DarazProducts{" +
                "daraz_sku_id='" + daraz_sku_id + '\'' +
                ", store_id='" + store_id + '\'' +
                ", primary_cat_id='" + primary_cat_id + '\'' +
                ", sku='" + sku + '\'' +
                ", attributes='" + attributes + '\'' +
                ", brand='" + brand + '\'' +
                ", updated_at=" + updated_at +
                ", mandatory_att=" + mandatory_att +
                ", mandatory_sku=" + mandatory_sku +
                ", create_status='" + create_status + '\'' +
                ", variant_id='" + variant_id + '\'' +
                ", technify_id='" + technify_id + '\'' +
                ", request_xml='" + request_xml + '\'' +
                ", response='" + response + '\'' +
                '}';
    }

    public static String getTableName(){
        return "darazproducts";
    }
}
