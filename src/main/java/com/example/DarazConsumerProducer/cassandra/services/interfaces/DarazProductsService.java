package com.example.DarazConsumerProducer.cassandra.services.interfaces;


import com.example.DarazConsumerProducer.cassandra.models.DarazProducts;

import java.util.List;

public interface DarazProductsService {

    DarazProducts createProducts(DarazProducts products);

    DarazProducts findBydaraz_Sku_id(String daraz_sku_id,String store_id);

    List<DarazProducts> findBydaraz_Sku_id_Status(String create_status);

    List<DarazProducts> getAllProducts();

}
