package com.example.DarazConsumerProducer.cassandra.services.implementations;

import com.example.DarazConsumerProducer.cassandra.dao.interfaces.DarazPushUpdates_InitialLogsDAO;
import com.example.DarazConsumerProducer.cassandra.models.DarazPushUpdates_Initial_Logs;
import com.example.DarazConsumerProducer.cassandra.services.interfaces.DarazPushUpdates_InitialLogsService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;

public class DarazPushUpdates_InitialLogsServiceImplementation implements DarazPushUpdates_InitialLogsService {


    @Autowired
    private DarazPushUpdates_InitialLogsDAO darazPushUpdatesDAO;

    @Override
    public DarazPushUpdates_Initial_Logs createProducts(DarazPushUpdates_Initial_Logs darazPushUpdates) {
        return darazPushUpdatesDAO.createProducts(darazPushUpdates);
    }

    @Override
    public List<DarazPushUpdates_Initial_Logs> findByStatus(String create_status)
    {
        HashMap<String,Object> hashMap = new HashMap<String,Object>();
        hashMap.put("status",create_status);
        return darazPushUpdatesDAO.findByStatus(hashMap);
    }


}
