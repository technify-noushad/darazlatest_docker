package com.example.DarazConsumerProducer.cassandra.services.implementations;



import com.example.DarazConsumerProducer.cassandra.dao.interfaces.UsersDAO;
import com.example.DarazConsumerProducer.cassandra.models.Users;
import com.example.DarazConsumerProducer.cassandra.services.interfaces.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;

public class UsersServiceImplementation implements UsersService {


    @Autowired
    private UsersDAO usersDAO;


    @Transactional
    public Users createUsers (Users users) {
        return usersDAO.createUsers(users);
    }

    @Transactional
    public Users findByStoreId(String store_id) {
        HashMap<String,Object> hashMap = new HashMap<String,Object>();
        hashMap.put("store_id",store_id);

        return usersDAO.findByStore_Id(hashMap);
    }

}
