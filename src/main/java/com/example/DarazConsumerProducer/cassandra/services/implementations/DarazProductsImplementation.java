package com.example.DarazConsumerProducer.cassandra.services.implementations;

import com.example.DarazConsumerProducer.cassandra.dao.interfaces.DarazProductsDAO;
import com.example.DarazConsumerProducer.cassandra.models.DarazProducts;
import com.example.DarazConsumerProducer.cassandra.services.interfaces.DarazProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

public class DarazProductsImplementation implements DarazProductsService {


    @Autowired
    private DarazProductsDAO darazProductsDAO;

    @Override
    public DarazProducts createProducts(DarazProducts products) {
        return darazProductsDAO.createProducts(products);
    }

    @Override
    @Transactional
    public DarazProducts findBydaraz_Sku_id(String daraz_sku_id, String store_id)
    {
        HashMap<String,Object> hashMap = new HashMap<String,Object>();
        hashMap.put("daraz_sku_id",daraz_sku_id);
        hashMap.put("store_id", store_id);
        return darazProductsDAO.findBydaraz_Sku_id_Status(hashMap);
    }

    @Override
    @Transactional
    public List<DarazProducts> findBydaraz_Sku_id_Status(String create_status){
        HashMap<String,Object> hashMap = new HashMap<String,Object>();
        hashMap.put("create_status",create_status);

        return darazProductsDAO.findBydaraz_Sku_id_Status1(hashMap);
    }

    @Override
    @Transactional
    public List<DarazProducts> getAllProducts()
    {
        return darazProductsDAO.getAllProducts();
    }

}
