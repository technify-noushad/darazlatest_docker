package com.example.DarazConsumerProducer.cassandra.services.interfaces;


import com.example.DarazConsumerProducer.cassandra.models.Users;

public interface UsersService {

    Users createUsers(Users users);

    Users findByStoreId(String store_id);

}
