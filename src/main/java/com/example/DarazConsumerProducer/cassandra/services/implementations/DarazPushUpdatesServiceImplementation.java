package com.example.DarazConsumerProducer.cassandra.services.implementations;

import com.example.DarazConsumerProducer.cassandra.dao.interfaces.DarazPushUpdatesDAO;
import com.example.DarazConsumerProducer.cassandra.models.DarazPushUpdates;
import com.example.DarazConsumerProducer.cassandra.services.interfaces.DarazPushUpdatesService;
import org.springframework.beans.factory.annotation.Autowired;

public class DarazPushUpdatesServiceImplementation implements DarazPushUpdatesService {


    @Autowired
    private DarazPushUpdatesDAO darazPushUpdatesDAO;

    @Override
    public DarazPushUpdates createProducts(DarazPushUpdates darazPushUpdates) {
        return darazPushUpdatesDAO.createProducts(darazPushUpdates);
    }

}
