package com.example.DarazConsumerProducer.cassandra.services.interfaces;


import com.example.DarazConsumerProducer.cassandra.models.DarazPushUpdates;

public interface DarazPushUpdatesService {

    DarazPushUpdates createProducts(DarazPushUpdates darazPushUpdates);

}
