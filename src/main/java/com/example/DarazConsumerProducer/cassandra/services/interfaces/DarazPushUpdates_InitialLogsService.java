package com.example.DarazConsumerProducer.cassandra.services.interfaces;

import com.example.DarazConsumerProducer.cassandra.models.DarazPushUpdates_Initial_Logs;

import java.util.List;

public interface DarazPushUpdates_InitialLogsService {

    DarazPushUpdates_Initial_Logs createProducts(DarazPushUpdates_Initial_Logs darazPushUpdates);

    List<DarazPushUpdates_Initial_Logs> findByStatus(String create_status);

}
