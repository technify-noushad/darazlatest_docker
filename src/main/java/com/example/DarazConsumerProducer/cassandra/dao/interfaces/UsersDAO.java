package com.example.DarazConsumerProducer.cassandra.dao.interfaces;


import com.example.DarazConsumerProducer.cassandra.models.Users;

import java.util.HashMap;

public interface UsersDAO {


    Users createUsers(Users users);

    Users findByStore_Id(HashMap<String, Object> hashMap);

}
