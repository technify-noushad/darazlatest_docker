package com.example.DarazConsumerProducer.cassandra.dao.implementations;


import com.example.DarazConsumerProducer.cassandra.MyCassandraTemplate;
import com.example.DarazConsumerProducer.cassandra.dao.interfaces.DarazPushUpdatesDAO;
import com.example.DarazConsumerProducer.cassandra.models.DarazPushUpdates;
import org.springframework.beans.factory.annotation.Autowired;


public class DarazPushUpdatesDAOImplementation implements DarazPushUpdatesDAO {

    @Autowired
    private MyCassandraTemplate myCassandraTemplate;

    @Override
    public DarazPushUpdates createProducts(DarazPushUpdates darazPushUpdates) {
        return myCassandraTemplate.create(darazPushUpdates);
    }

}
