package com.example.DarazConsumerProducer.cassandra.dao.implementations;


import com.example.DarazConsumerProducer.cassandra.MyCassandraTemplate;
import com.example.DarazConsumerProducer.cassandra.dao.interfaces.UsersDAO;
import com.example.DarazConsumerProducer.cassandra.models.Users;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;

public class UsersDAOImplementation implements UsersDAO {

    @Autowired
    private MyCassandraTemplate myCassandraTemplate;


    public Users createUsers(Users users) {
        return myCassandraTemplate.create(users);
    }

    public Users findByStore_Id(HashMap<String,Object> hashMap) {
        return myCassandraTemplate.findOneBy(hashMap,Users.getTableName(),Users.class);
    }
}
