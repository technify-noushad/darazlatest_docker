package com.example.DarazConsumerProducer.cassandra.dao.interfaces;

import com.example.DarazConsumerProducer.cassandra.models.DarazProducts;

import java.util.HashMap;
import java.util.List;

public interface DarazProductsDAO {


    DarazProducts createProducts(DarazProducts products);

    DarazProducts findBydaraz_Sku_id_Status(HashMap<String, Object> hashMap);

    List<DarazProducts> findBydaraz_Sku_id_Status1(HashMap<String, Object> hashMap);

    List<DarazProducts> getAllProducts();

}
