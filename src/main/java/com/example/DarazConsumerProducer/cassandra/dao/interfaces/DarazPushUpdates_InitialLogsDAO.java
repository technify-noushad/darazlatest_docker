package com.example.DarazConsumerProducer.cassandra.dao.interfaces;

import com.example.DarazConsumerProducer.cassandra.models.DarazPushUpdates_Initial_Logs;

import java.util.HashMap;
import java.util.List;

public interface DarazPushUpdates_InitialLogsDAO {

    DarazPushUpdates_Initial_Logs createProducts(DarazPushUpdates_Initial_Logs darazPushUpdates);

    List<DarazPushUpdates_Initial_Logs> findByStatus(HashMap<String, Object> hashMap);

}
