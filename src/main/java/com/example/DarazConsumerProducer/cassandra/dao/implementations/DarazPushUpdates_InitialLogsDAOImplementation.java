package com.example.DarazConsumerProducer.cassandra.dao.implementations;


import com.example.DarazConsumerProducer.cassandra.MyCassandraTemplate;
import com.example.DarazConsumerProducer.cassandra.dao.interfaces.DarazPushUpdates_InitialLogsDAO;
import com.example.DarazConsumerProducer.cassandra.models.DarazProducts;
import com.example.DarazConsumerProducer.cassandra.models.DarazPushUpdates_Initial_Logs;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;


public class DarazPushUpdates_InitialLogsDAOImplementation implements DarazPushUpdates_InitialLogsDAO {

    @Autowired
    private MyCassandraTemplate myCassandraTemplate;

    @Override
    public DarazPushUpdates_Initial_Logs createProducts(DarazPushUpdates_Initial_Logs darazPushUpdates) {
        return myCassandraTemplate.create(darazPushUpdates);
    }

    @Override
    public List<DarazPushUpdates_Initial_Logs> findByStatus(HashMap<String, Object> hashMap)
    {
        return myCassandraTemplate.findAllBy(hashMap,DarazPushUpdates_Initial_Logs.getTableName(),1000, DarazPushUpdates_Initial_Logs.class);
    }

}
