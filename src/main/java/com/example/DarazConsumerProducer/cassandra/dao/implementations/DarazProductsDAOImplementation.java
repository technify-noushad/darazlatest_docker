package com.example.DarazConsumerProducer.cassandra.dao.implementations;


import com.example.DarazConsumerProducer.cassandra.MyCassandraTemplate;
import com.example.DarazConsumerProducer.cassandra.dao.interfaces.DarazProductsDAO;
import com.example.DarazConsumerProducer.cassandra.models.DarazProducts;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;

public class DarazProductsDAOImplementation implements DarazProductsDAO {

    @Autowired
    private MyCassandraTemplate myCassandraTemplate;

    @Override
    public DarazProducts createProducts(DarazProducts products) {
        return myCassandraTemplate.create(products);
    }

    @Override
    public DarazProducts findBydaraz_Sku_id_Status(HashMap<String, Object> hashMap) {
        return myCassandraTemplate.findOneBy(hashMap,DarazProducts.getTableName(),DarazProducts.class);
    }

    @Override
    public List<DarazProducts> findBydaraz_Sku_id_Status1(HashMap<String, Object> hashMap) {
        return myCassandraTemplate.findAllBy(hashMap,DarazProducts.getTableName(),4000, DarazProducts.class);
    }

    @Override
    public List<DarazProducts> getAllProducts()
    {
        return myCassandraTemplate.getAll(DarazProducts.getTableName(), DarazProducts.class);
    }

}
