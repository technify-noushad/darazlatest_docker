package com.example.DarazConsumerProducer.cassandra;

import com.datastax.driver.core.SocketOptions;
import com.example.DarazConsumerProducer.Constants.ProjectConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.config.CassandraSessionFactoryBean;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.data.cassandra.core.convert.CassandraConverter;
import org.springframework.data.cassandra.core.convert.MappingCassandraConverter;
import org.springframework.data.cassandra.core.mapping.BasicCassandraMappingContext;
import org.springframework.data.cassandra.core.mapping.CassandraMappingContext;


@Configuration
public class CassandraUtil {


    public CassandraUtil() {
        System.out.println("DarazConnector => CassandraUtil()");
    }

    private String getKeyspaceName() {
        return "darazconnector";
        // return environment.getProperty(KEYSPACE);
    }

    private String getContactPoints() {
        return ProjectConstants.cassandraEndPoint;
    }

    private int getPortNumber() {
        return ProjectConstants.cassandraPort;
    }

    @Bean
    public CassandraClusterFactoryBean cluster() {
        CassandraClusterFactoryBean cluster = new CassandraClusterFactoryBean();
        cluster.setContactPoints(getContactPoints());
        cluster.setPort(getPortNumber());
        cluster.setUsername(ProjectConstants.cassandraUsername);
        cluster.setPassword(ProjectConstants.cassandraPass);
        SocketOptions so = new SocketOptions();
        so.setConnectTimeoutMillis(10000);
        so.setReadTimeoutMillis(200000);
        cluster.setSocketOptions(so);
        return cluster;
    }

    @Bean
    public CassandraMappingContext mappingContext() {
        return new BasicCassandraMappingContext();
    }

    @Bean
    public CassandraConverter converter() {
        return new MappingCassandraConverter(mappingContext());
    }

    @Bean
    public CassandraSessionFactoryBean session() throws Exception {
        CassandraSessionFactoryBean cassandraSessionFactoryBean = new CassandraSessionFactoryBean();
        cassandraSessionFactoryBean.setCluster(cluster().getObject());
        cassandraSessionFactoryBean.setKeyspaceName(getKeyspaceName());
        cassandraSessionFactoryBean.setConverter(converter());
        cassandraSessionFactoryBean.setSchemaAction(SchemaAction.NONE);
        return cassandraSessionFactoryBean;
    }

    @Bean
    public CassandraOperations cassandraTemplate() throws Exception {
        return new CassandraTemplate(session().getObject());
    }


}