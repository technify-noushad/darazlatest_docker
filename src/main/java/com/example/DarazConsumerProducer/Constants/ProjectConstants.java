package com.example.DarazConsumerProducer.Constants;

public class ProjectConstants {
    public static final String KAFKA_DISTRIBUTED = "http://technify.tech:62111/";
    public static long secondInMilli = 1000;
    public static long minuteInMilli = 60000;
    public static final String HASH_ALGORITHM = "HmacSHA256";
    public static final String HASH_ALGORITHM1 = "sha256";
    public static final String CHAR_UTF_8 = "UTF-8";
    public static final String CHAR_ASCII = "ASCII";
    public static boolean isRunningLocally = false;

    public static String elasticUserName = "elastic";
    public static String elasticPass = "5Rivers..";
    public static String elasticEndPoint = "technify.tech";
    public static String elasticPort = "64017";
//    public static String elasticEndPoint = "10.142.0.6,10.142.0.7,10.142.0.10";
//    public static String elasticPort = "9200";

/*Cassandra Database*/
    public static final String cassandraKeySpace        =   "darazconnector";
//    public static final String cassandraEndPoint        =   "10.128.0.4,10.142.0.4,10.142.0.3";
//    public static final int    cassandraPort            =   9042;
    public static final String cassandraEndPoint        =   "technify.tech";
    public static final int   cassandraPort             =   64009;
    public static final String cassandraUsername        =   "cassandra";
    public static final String cassandraPass            =   "5Rivers..";

    public static final String cassandraKeySpace_test        =   "darazconnector";
    public static final String cassandraEndPoint_test        =   "10.0.0.249";
    public static final int    cassandraPort_test            =   9042;
    public static final String cassandraUsername_test        =   "khawar";
    public static final String cassandraPass_test            =   "5Rivers..";

    /*Images Folder Path*/
    public static String FolderPath = "/home/noushad/Desktop/LazadaImagesFolder/";
    public static String testServerPath = "/home/khawar/affiliates/lazada_connector/images/";
    public static String finalServerPath = "/home/kunit.technify/affiliates/LazadaConnector/images/";

    /* NIFI Speculator
     * */
    public static final String getSpecFromElastic             =   "http://service.technify.tech/spec/spec-api/spec/get";
    public static final String darazsizeSpecEntityKey        =   "6a2b1f2a9c6311e898d0529269fb1459_size-migrations-product";
    public static final String darazColorSpecEntityKey        =   "6a2b1f2a9c6311e898d0529269fb1459_color-migrations-product";
    public static final String darazStoresizeSpecEntityKey        =   "_size-migrations-product";
    public static final String currencyConversionEntityKey    =   "currencyConversion-migrations-product";
    public static final String getTransformSpecFromelastic    =   "http://service.technify.tech/spec/spec-api/spec/transform?";
    public static final String nifiStoreIDEntityKey           =   "storeid-entity";
    public static final String categoryMappingEntity          =    "_DARAZ-category-mapping";
    public static final String darazProjectMigration         =   "6a2b1f2a9c6311e898d0529269fb1459-migrations-product";
    public static final String darazMigrationEntity          =    "_DARAZ-migrations-product";

    /*
     *   Logs
     * */
    public static final String devName   = "Noushad Tabani";
    public static final String projectName = "Daraz Connector";
    public static final String test_logURL = "http://10.0.0.248:8082/topics/logs";
    public static final String live_LogURL =  "http://10.148.0.2:62111/topics/logs";
    public static final String liveIP2    = "http://technify.tech:62111/topics/logs";


    public static final String refMessage = "&lt;ul&gt; &lt;li&gt; For Size Reference, Check Images. &lt;/li&gt; &lt;/ul&gt;";
}