package com.example.DarazConsumerProducer.Main;

import com.example.DarazConsumerProducer.Constants.ProjectConstants;
import com.example.DarazConsumerProducer.DarazSDK.DarazAPIClass;
import com.example.DarazConsumerProducer.HelperClasses.CustomImageHandler;
import com.example.DarazConsumerProducer.HelperClasses.HelpingUtilities;
import com.example.DarazConsumerProducer.cassandra.models.DarazProducts;
import com.example.DarazConsumerProducer.cassandra.models.DarazPushUpdates;
import com.example.DarazConsumerProducer.cassandra.models.DarazPushUpdates_Initial_Logs;
import com.example.DarazConsumerProducer.cassandra.models.Users;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jnr.ffi.Struct;
import org.apache.kafka.common.errors.ApiException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.example.DarazConsumerProducer.Main.MainApplicationClass.*;

public class WorkingClass {

    DarazAPIClass darazAPIClass = new DarazAPIClass();

    ArrayList<DarazProducts> darazProductsArrayList = new ArrayList<>();
    ArrayList<JSONObject> optionalFieldsAttributes = new ArrayList<>();
    ArrayList<JSONObject> mandatoryFieldsAttributes = new ArrayList<>();
    ArrayList<JSONObject> optionalFieldsSKU = new ArrayList<>();
    ArrayList<JSONObject> mandatoryFieldsSKU= new ArrayList<>();
    List<String> mandatory_attr = new ArrayList<>();
    List<String> mandatory_sku = new ArrayList<>();
    JSONObject sizeObject ;
    JSONObject colorObject;

    public void doWork(LinkedHashMap<String, Object> productMap, String store_id, Users users, JSONObject categoriesMapped)
    {

        sizeObject = getSpecs(store_id+ProjectConstants.darazStoresizeSpecEntityKey);
        if(sizeObject == null)
        {
            sizeObject = getSpecs(ProjectConstants.darazsizeSpecEntityKey);
        }
        colorObject = getSpecs(ProjectConstants.darazColorSpecEntityKey);

        LinkedHashMap<String, Object> jsonDetails = productMap;
        ArrayList<String> categories = (ArrayList<String>) productMap.get("store_product_category");

        if (categories != null) {
            String mappedCategory = getMapping(categories, categoriesMapped);

            if (!mappedCategory.equals("NONE")) {
                productMap = new WorkingClass().getTransformedProduct(productMap, users.getStore_id());
                System.out.println("DarazConnector =>" + productMap);

                //JSONObject proJsonObject = new JSONObject(productMap);
                JSONObject proJsonObject = updateAlreadyExistProducts(productMap, users, jsonDetails, store_id);

                if (proJsonObject != null && proJsonObject.getJSONArray("variants").length() > 0) {
                    String productBody = "";
                    try {
                        productBody = createProducts(proJsonObject, mappedCategory, store_id, users, false);
                        System.out.println("DarazConnector =>" + productBody);

                        if(!productBody.equals("")) {
                            String response = darazAPIClass.CreateProduct1(users.getUser_id(), users.getApi_key(), productBody);

                            System.out.println("DarazConnector =>" + "DarazConnector => createProductResponse-> " + response);
                            JSONObject respObject = new JSONObject(response);
                            if (respObject.has("SuccessResponse")) {
                                SetDarazProductsCassandra_Logs("1", productBody, response,"1");
                            } else {
                                SetDarazProductsCassandra_Logs("0", productBody, response, "0");
                            }
                        }
                    } catch (ApiException e) {
                        e.printStackTrace();
                        System.out.println("DarazConnector =>" + "DarazConnector =>" + e.getMessage());
                        System.out.println("DarazConnector =>" + HelpingUtilities.StackTraceToString(e));

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        System.out.println("DarazConnector =>" + HelpingUtilities.StackTraceToString(e));
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (TransformerException e) {
                        e.printStackTrace();
                    } catch (ParserConfigurationException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }

    private String getMapping(ArrayList<String> productCategories, JSONObject categoriesMapped)
    {
        String mapped = "NONE";
        for(int i=0; i<productCategories.size(); i++)
        {
            if(categoriesMapped.has(productCategories.get(i)))
            {
                mapped = categoriesMapped.getString(productCategories.get(i));
                break;
            }
        }
        return mapped;
    }

    private String generateUUID()
    {
        long msb = System.currentTimeMillis();
        long lsb = System.currentTimeMillis();
        //initialize uuid
        UUID uuid = new UUID(msb, lsb);
        return uuid.toString();
    }

    private void SetDarazProductsCassandra_Logs(String status, String productBody, String response, String is_active)
    {
        System.out.println("DarazConnector =>" + "DarazConnector => SetDarazProductsCassandra");
        for(int p=0; p<darazProductsArrayList.size(); p++)
        {
            darazProductsArrayList.get(p).setRequest_xml(productBody);
            darazProductsArrayList.get(p).setResponse(response);
            darazProductsArrayList.get(p).setCreate_status(status);
            darazProductsArrayList.get(p).setIs_active(is_active);


            darazProductsService.createProducts(darazProductsArrayList.get(p));
        }

        darazProductsArrayList.removeAll(darazProductsArrayList);
        System.out.println("DarazConnector =>" + "DarazConnector => SetDarazProductsCassandra => DONE");
    }

    public void pushUpdates(LinkedHashMap<String, Object> productMap)
    {
        String store_id =  productMap.get("store_id").toString();

        Users users =  usersService.findByStoreId(store_id);

        if(users != null) {
            String product = new Gson().toJson(productMap.get("product"));
            LinkedHashMap<String, Object> jsonDetails = new Gson().fromJson(product, new TypeToken<LinkedHashMap<String, Object>>() {
            }.getType());

            //JSONObject object = new JSONObject(productMap.get("product"));
            System.out.println("DarazConnector =>" + "DarazConnector => pushUpdates" + jsonDetails);
            productMap = new WorkingClass().getTransformedProduct(jsonDetails, store_id);
            updateAlreadyExistProducts(productMap, users, jsonDetails, store_id);
        }
    }
    private JSONObject updateAlreadyExistProducts(LinkedHashMap<String, Object> productMap, Users users, LinkedHashMap<String, Object> jsonDetails, String store_id)
    {
        boolean insert = false;
        System.out.println("DarazConnector =>" + "DarazConnector => updateAlreadyExistProducts");
        try{
            JSONObject productJson = new JSONObject(productMap);
            System.out.println("DarazConnector =>" + "DarazConnector => productJson ->" + productJson);
            JSONArray variants = productJson.getJSONArray("variants");
            for(int i=0; i<variants.length(); i++) {
                String darazSKU = variants.getJSONObject(i).get("SellerSku").toString();
                int SKULength = darazSKU.length();
                if (SKULength > 50) {
                    darazSKU = darazSKU.substring(SKULength - 50, SKULength);
                    JSONObject jsonObject = variants.getJSONObject(i);
                    jsonObject.put("SellerSku", darazSKU);
                    variants.put(i, jsonObject);
                }
                System.out.println("DarazConnector =>" + "DarazConnector => " + darazSKU);

                DarazProducts darazProducts = darazProductsService.findBydaraz_Sku_id(darazSKU, store_id);

                if (darazProducts != null) {
                    System.out.println("DarazConnector =>" + darazProducts.toString());
                    if (darazProducts.getCreate_status().equals("1")) {
                        int isActive = (int) Double.parseDouble(productJson.get("status").toString());

                        if(productJson.has("status") && !darazProducts.getIs_active().equals(String.valueOf(isActive)))
                        {
                            JSONObject obj = new WorkingClass().getSpecs(users.getStore_id()+ProjectConstants.categoryMappingEntity);
                            doWork_Update(jsonDetails,users.getStore_id(),users,obj);
                            insert = true;
                            break;
                        }
                        else {
                            updateSpecificVariant(variants.getJSONObject(i), darazProducts, users);
                        }
                    } else {
                        if (jsonDetails != null) {
                            JSONObject obj = new WorkingClass().getSpecs(users.getStore_id() + ProjectConstants.categoryMappingEntity);
                            insertProductPending(jsonDetails, users.getStore_id(), users, obj);
                            insert = true;
                            break;
                        }
                    }
                    variants.remove(i);
                    i--;
                }
                else
                {
                    JSONObject obj = new WorkingClass().getSpecs(users.getStore_id() + ProjectConstants.categoryMappingEntity);
                    insertProductPending(jsonDetails, users.getStore_id(), users, obj);
                    insert = true;
                }
            }
            if(insert)
                return null;

            return productJson;
        }
        catch (Exception e)
        {
            System.out.println("DarazConnector =>" + "DarazConnector => updateAlreadyExistProducts" + e.getMessage());
        }
        return null;
    }

    public void doWork_Update(LinkedHashMap<String, Object> productMap, String store_id, Users users, JSONObject categoriesMapped) {

        sizeObject = getSpecs(store_id + ProjectConstants.darazStoresizeSpecEntityKey);
        if (sizeObject == null) {
            sizeObject = getSpecs(ProjectConstants.darazsizeSpecEntityKey);
        }
        colorObject = getSpecs(ProjectConstants.darazColorSpecEntityKey);

        LinkedHashMap<String, Object> jsonDetails = productMap;
        ArrayList<String> categories = (ArrayList<String>) productMap.get("store_product_category");

        if (categories != null) {
            String mappedCategory = getMapping(categories, categoriesMapped);

            if (!mappedCategory.equals("NONE")) {
                productMap = new WorkingClass().getTransformedProduct(productMap, users.getStore_id());
                System.out.println("DarazConnector =>" + productMap);

                //JSONObject proJsonObject = new JSONObject(productMap);
                JSONObject proJsonObject = new JSONObject(productMap);

                if (proJsonObject != null && proJsonObject.getJSONArray("variants").length() > 0) {
                    String productBody = "";
                    try {
                        productBody = createProducts(proJsonObject, mappedCategory, store_id, users, true);
                        System.out.println("DarazConnector =>" + productBody);

                        if (!productBody.equals("")) {
                            String response = darazAPIClass.UpdateProduct(users.getUser_id(), users.getApi_key(), productBody);

                            System.out.println("DarazConnector =>" + "DarazConnector => createProductResponse-> " + response);
                            JSONObject respObject = new JSONObject(response);

                            int is_active = (int) Double.parseDouble(proJsonObject.get("status").toString());
                            if (respObject.has("SuccessResponse")) {
                                SetDarazProductsCassandra_Logs("1", productBody, response, String.valueOf(is_active));
                            } else {
                                SetDarazProductsCassandra_Logs("0", productBody, response, String.valueOf(0));
                            }
                        }
                    } catch (ApiException e) {
                        e.printStackTrace();
                        System.out.println("DarazConnector =>" + "DarazConnector =>" + e.getMessage());
                        System.out.println("DarazConnector =>" + HelpingUtilities.StackTraceToString(e));

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        System.out.println("DarazConnector =>" + HelpingUtilities.StackTraceToString(e));
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (TransformerException e) {
                        e.printStackTrace();
                    } catch (ParserConfigurationException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }

    private void insertProductPending(LinkedHashMap<String, Object> productMap, String store_id, Users users, JSONObject categoriesMapped)
    {
        System.out.println("DarazConnector =>" + "DARAZ CONNECTOR => insertProductPending");

        String status = productMap.get("status").toString();
        String quan = productMap.get("quantity").toString();
        if (!status.equals("0")) {

            ArrayList<String> categories = (ArrayList<String>) productMap.get("store_product_category");

            if (categories != null) {
                String mappedCategory = getMapping(categories, categoriesMapped);

                if (!mappedCategory.equals("NONE")) {

                    productMap = new WorkingClass().getTransformedProduct(productMap, store_id);
                    JSONObject proJsonObject = new JSONObject(productMap);
                    JSONArray variants = proJsonObject.getJSONArray("variants");
                    for (int i = 0; i < variants.length(); i++) {
                        String darazSKU = variants.getJSONObject(i).get("SellerSku").toString();
                        int SKULength = darazSKU.length();
                        if (SKULength > 50) {
                            darazSKU = darazSKU.substring(SKULength - 50, SKULength);
                            JSONObject jsonObject = variants.getJSONObject(i);
                            jsonObject.put("SellerSku", darazSKU);
                            variants.put(i, jsonObject);
                        }
                    }

                    if (proJsonObject.getJSONArray("variants").length() > 0) {
                        String productBody = null;
                        try {

                            productBody = createProducts(proJsonObject, mappedCategory, store_id, users,false);
                            System.out.println("DarazConnector =>" + productBody);
                            if (productBody.equals("")) {

                            } else {
                                String response = darazAPIClass.CreateProduct(users.getUser_id(), users.getApi_key(), productBody);
                                System.out.println("DarazConnector =>" + "DarazConnector => createProductResponse-> " + response);
                                JSONObject respObject = new JSONObject(response);
                                if (respObject.has("SuccessResponse")) {
                                    SetDarazProductsCassandra_Logs("1", productBody, response,"1");
                                } else {
                                    SetDarazProductsCassandra_Logs("0", productBody, response,"0");
                                }

                            }
                        } catch (ApiException e) {
                            e.printStackTrace();
                            System.out.println("DarazConnector =>" + "DarazConnector =>" + e.getMessage());
                            System.out.println("DarazConnector =>" + HelpingUtilities.StackTraceToString(e));
                            SetDarazProductsCassandra_Logs("0", productBody, e.getMessage(),"0");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            System.out.println("DarazConnector =>" + HelpingUtilities.StackTraceToString(e));
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (TransformerException e) {
                            e.printStackTrace();
                        } catch (ParserConfigurationException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    private void updateSpecificVariant(JSONObject object, DarazProducts darazProducts, Users users)
    {
        boolean special_price = false;
        if(object.has("product_specials")) {
            JSONArray jsonArray = object.getJSONArray("product_specials");
            if (jsonArray.length() > 0) {
                JSONObject specialObject = jsonArray.getJSONObject(0);
                if (specialObject.has("special_price")) {
                    special_price = true;
                    double pr = Double.parseDouble(specialObject.get("special_price").toString());
                    int sp_price = (int)pr;
                    object.put("special_price", String.valueOf(sp_price));

                    String pattern = "yyyy-MM-dd";
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

                    Date fromDate = new Date();
                    Calendar c = Calendar.getInstance();
                    c.setTime(fromDate);
                    c.add(Calendar.WEEK_OF_MONTH, 4);
                    Date toDate = c.getTime();

                    String fromDate1 = simpleDateFormat.format(fromDate);
                    String toDate1 = simpleDateFormat.format(toDate);
                    //System.out.println(toDate1);

                    if (specialObject.get("special_from_date").toString().contains("0000") || specialObject.get("special_from_date").toString().equals("")) {
                        object.put("special_from_date", fromDate1);
                        object.put("special_to_date", toDate1);
                    } else {
                        object.put("special_from_date", specialObject.get("special_from_date"));
                        object.put("special_to_date", specialObject.get("special_to_date"));
                    }
                }
            }
        }
        try {
            object.put("price", object.get("price").toString());

            System.out.println("DarazConnector =>" + "DarazConnector => updateSpecificVariant");
            int quantity = (int) Double.parseDouble(object.get("quantity").toString());

            System.out.println("DarazConnector =>" + "DarazConnector => updateSpecificVariant quantity" + quantity);
            /*String xmlBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "\n" +
                    "<Request>   \n" +
                    " <Product>     \n" +
                    "  <Skus>       \n" +
                    "   <Sku>         \n" +
                    "    <SellerSku>" + object.get("SellerSku").toString() + " </SellerSku>         \n" +
                    "    <Quantity>" + quantity + "</Quantity>         \n" +//+
                    "    <Price>" + object.get("price").toString() + "</Price>       \n" +
                    "    <SalePrice/>         \n" +
                    "    <SaleStartDate/>         \n" +
                    "    <SaleEndDate/>       \n" +
                    "   </Sku>       \n" +
                    "  </Skus>   \n" +
                    " </Product> \n" +
                    "</Request>";*/

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            // Request element
            Element request = document.createElement("Request");
            document.appendChild(request);

            // Product element
            Element product = document.createElement("Product");
            request.appendChild(product);

            // Skus element
            Element skus = document.createElement("Skus");
            product.appendChild(skus);

            // Sku element
            Element sku = document.createElement("Sku");
            skus.appendChild(sku);

            // SellerSku element
            String S_Sku = object.get("SellerSku").toString();
            S_Sku = S_Sku.replace(" & ", "and");
            Element SellerSku = document.createElement("SellerSku");
            SellerSku.appendChild(document.createTextNode(S_Sku));
            sku.appendChild(SellerSku);

            // Quantity element
            Element Quantity = document.createElement("Quantity");
            Quantity.appendChild(document.createTextNode(String.valueOf(quantity)));
            sku.appendChild(Quantity);

            // Price element
            Element Price = document.createElement("Price");
            Price.appendChild(document.createTextNode(object.get("price").toString()));
            sku.appendChild(Price);

            // SalePrice element
            Element SalePrice = document.createElement("SalePrice");
            if(special_price && object.has("special_price"))
                SalePrice.appendChild(document.createTextNode(object.get("special_price").toString()));
            sku.appendChild(SalePrice);

            // SaleStartDate element
            Element SaleStartDate = document.createElement("SaleStartDate");
            if(special_price && object.has("special_price"))
                SaleStartDate.appendChild(document.createTextNode(object.get("special_from_date").toString()));
            sku.appendChild(SaleStartDate);

            // SaleEndDate element
            Element SaleEndDate = document.createElement("SaleEndDate");
            if(special_price && object.has("special_price"))
                SaleEndDate.appendChild(document.createTextNode(object.get("special_to_date").toString()));
            sku.appendChild(SaleEndDate);

            // create the xml file
            //transform the DOM Object to an XML File
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
//        StreamResult streamResult = new StreamResult(new File(xmlFilePath));

            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            transformer.transform(domSource, result);
            System.out.println(writer.toString());
            String xmlBody = writer.toString();
            System.out.println(xmlBody);

            String response = darazAPIClass.UpdatePriceQuantity(users.getUser_id(), users.getApi_key(), xmlBody);
            System.out.println("DarazConnector =>" + "DarazConnector => updateSpecificVariant response " + response);

            DarazPushUpdates darazPushUpdates = new DarazPushUpdates(generateUUID(), darazProducts.getStore_id(), darazProducts.getDaraz_sku_id(),
                    xmlBody, response, new Timestamp(System.currentTimeMillis()), object.get("variant_id").toString(), object.get("Seller").toString());

            darazPushUpdatesService.createProducts(darazPushUpdates);

            if(response.contains("ErrorResponse"))
            {
                if(response.contains("429"))
                {
                    DarazPushUpdates_Initial_Logs darazPushUpdates_initial_logs = new DarazPushUpdates_Initial_Logs(darazProducts.getStore_id(), darazProducts.getDaraz_sku_id(),
                            xmlBody, response, new Timestamp(System.currentTimeMillis()), object.get("variant_id").toString(), object.get("Seller").toString(),"0");
                    darazPushUpdates_initialLogsService.createProducts(darazPushUpdates_initial_logs);
                }
                else {


                    JSONObject respObject = new JSONObject(response);
                    String message = respObject.getJSONObject("ErrorResponse").getJSONObject("Body").getJSONArray("Errors").getJSONObject(0).get("Message").toString();
                    if (message.contains("Negative")) {
                        String[] temp = message.split(" ");
                        for (int i = 0; i < temp.length; i++) {
                            if (temp[i].toLowerCase().contains("reserved") && temp[i + 1].toLowerCase().contains("stock")) {

                                sku.removeChild(Quantity);

                                Element Quantity1 = document.createElement("Quantity");
                                Quantity1.appendChild(document.createTextNode(temp[i + 2]));
                                sku.appendChild(Quantity1);


                                TransformerFactory transformerFactory1 = TransformerFactory.newInstance();
                                Transformer transformer1 = transformerFactory1.newTransformer();
                                DOMSource domSource1 = new DOMSource(document);

                                StringWriter writer1 = new StringWriter();
                                StreamResult result1 = new StreamResult(writer1);
                                transformer1.transform(domSource1, result1);
                                System.out.println(writer1.toString());
                                xmlBody = writer1.toString();
                                System.out.println(xmlBody);

                                response = darazAPIClass.UpdatePriceQuantity(users.getUser_id(), users.getApi_key(), xmlBody);

                                darazPushUpdates = new DarazPushUpdates(generateUUID(), darazProducts.getStore_id(), darazProducts.getDaraz_sku_id(),
                                        xmlBody, response, new Timestamp(System.currentTimeMillis()), object.get("variant_id").toString(), object.get("Seller").toString());

                                darazPushUpdatesService.createProducts(darazPushUpdates);
                                break;
                            }
                        }
                    }
                }
            }
            System.out.println("DarazConnector =>" + "DarazConnector => updateSpecificVariant DONE ");
        }
        catch (Exception e)
        {
            System.out.println("DarazConnector =>" + "DarazConnector => updateSpecificVariant "+ e.getMessage());
            System.out.println("DarazConnector =>" + "DARAZ EXCEPTION =>>> " + HelpingUtilities.StackTraceToString(e));
        }
    }

    private void emptyAllLists()
    {
        mandatoryFieldsSKU.removeAll(mandatoryFieldsSKU);
        mandatoryFieldsAttributes.removeAll(mandatoryFieldsAttributes);
        optionalFieldsAttributes.removeAll(optionalFieldsAttributes);
        optionalFieldsSKU.removeAll(optionalFieldsSKU);
        mandatory_attr.removeAll(mandatory_attr);
        mandatory_sku.removeAll(mandatory_sku);
    }

    private boolean getCategoryAttributes(String cat_id, Users users)
    {
        System.out.println("DarazConnector =>" + "DarazConnector => getCategoryAttributes");
        emptyAllLists();

        String attributes = darazAPIClass.GetCategoryAttributes(users.getUser_id(),users.getApi_key(), "",cat_id);//.getCategoryAttributes(cat_id, LazadaAPIUrl);//(categoryMapping.getLazada_cat_id());

        JSONObject jsonObject = new JSONObject(attributes);
        if(jsonObject.has("SuccessResponse"))
        {
            JSONArray data = jsonObject.getJSONObject("SuccessResponse").getJSONArray("Body");
            if(data.length()>0) {
                for (int obj = 0; obj < data.length(); obj++) {
                    JSONObject object = data.getJSONObject(obj);
                    if (object.get("isMandatory") != null && object.get("isMandatory").toString().equals("1")) {
                        if (object.get("attributeType") != null && object.get("attributeType").toString().toLowerCase().equals("sku")) {
                            mandatoryFieldsSKU.add(object);
                            mandatory_sku.add(object.get("name").toString());
                        } else if (object.get("attributeType") != null) {
                            mandatoryFieldsAttributes.add(object);
                            mandatory_attr.add(object.get("name").toString());
                        }
                    } else {
                        if (object.get("attributeType") != null && object.get("attributeType").toString().toLowerCase().equals("sku")) {
                            optionalFieldsSKU.add(object);
                        } else if (object.get("attributeType") != null) {
                            optionalFieldsAttributes.add(object);
                        }
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    private LinkedHashMap<String, Object> getTransformedProduct(LinkedHashMap<String, Object> productMap, String store_id)
    {
        System.out.println("DarazConnector =>" + "DarazConnector => getTransformedProduct");
        String output = "";
        try {
            JSONObject object = new JSONObject(productMap);
            System.out.println("DarazConnector =>" + "DarazConnector=> "+ object);
            String json = object.toString();
            URL url = new URL(ProjectConstants.getTransformSpecFromelastic+ProjectConstants.nifiStoreIDEntityKey+ "=" +store_id+ ProjectConstants.darazMigrationEntity);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            //Setting the Request Method header as POST
            conn.setRequestMethod("POST");

            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty(ProjectConstants.nifiStoreIDEntityKey, store_id+ProjectConstants.darazMigrationEntity);
            conn.setRequestProperty("Authorization", "Basic Nkp0M0U2ajRWeEdMcjZ4SjprQ2dTQ1Q2RDVDc2VVaDNh");

            OutputStream os = conn.getOutputStream();
            os.write(object.toString().getBytes());
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                System.out.println("DarazConnector =>" + conn.getResponseMessage() + " Successfully Retrieved Transformed Spec");
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));


            System.out.println("DarazConnector =>" + "Output from Server .... \n");

            String line = "";
            while ((line = br.readLine()) != null) {
                output = line ;
                System.out.println("DarazConnector =>" + output);
            }

            conn.disconnect();

            LinkedHashMap<String, Object> jsonDetails = new Gson().fromJson(output, new TypeToken<LinkedHashMap<String, Object>>() {}.getType());

            System.out.println("DarazConnector =>" + "DarazConnector => TransformedProduct ->" + jsonDetails);
            return jsonDetails;
        } catch (MalformedURLException e) {

        } catch (IOException e) {

        }

        return null;
    }

    private LinkedHashMap<String, Object> getTransformedProduct_Update(JSONObject object)
    {
        System.out.println("DarazConnector =>" + "DarazConnector => getTransformedProduct");
        String output = "";
        try {
            System.out.println("DarazConnector =>" + "DarazConnector=> "+ object);
            String json = object.toString();
            URL url = new URL(ProjectConstants.getTransformSpecFromelastic+ProjectConstants.nifiStoreIDEntityKey+ "=" + ProjectConstants.darazProjectMigration);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            //Setting the Request Method header as POST
            conn.setRequestMethod("POST");

            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty(ProjectConstants.nifiStoreIDEntityKey, ProjectConstants.darazProjectMigration);
            conn.setRequestProperty("Authorization", "Basic Nkp0M0U2ajRWeEdMcjZ4SjprQ2dTQ1Q2RDVDc2VVaDNh");

            OutputStream os = conn.getOutputStream();
            os.write(object.toString().getBytes());
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                System.out.println("DarazConnector =>" + conn.getResponseMessage() + " Successfully Retrieved Transformed Spec");
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));


            System.out.println("DarazConnector =>" + "Output from Server .... \n");

            String line = "";
            while ((line = br.readLine()) != null) {
                output = line ;
                System.out.println("DarazConnector =>" + output);
            }

            conn.disconnect();

            LinkedHashMap<String, Object> jsonDetails = new Gson().fromJson(output, new TypeToken<LinkedHashMap<String, Object>>() {}.getType());

            System.out.println("DarazConnector =>" + "DarazConnector => TransformedProduct ->" + jsonDetails);
            return jsonDetails;
        } catch (MalformedURLException e) {

        } catch (IOException e) {

        }

        return null;
    }

    public JSONObject getSpecs(String ConstantEntity)
    {
        String output = "";
        try {

            URL url = new URL(ProjectConstants.getSpecFromElastic);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            //Setting the Request Method header as POST
            conn.setRequestMethod("GET");

            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty(ProjectConstants.nifiStoreIDEntityKey, ConstantEntity);
            conn.setRequestProperty("Authorization", "Basic Nkp0M0U2ajRWeEdMcjZ4SjprQ2dTQ1Q2RDVDc2VVaDNh");

            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                System.out.println("DarazConnector =>" + conn.getResponseMessage() + " Successfully Retrieved Transformed Spec");
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));


            System.out.println("DarazConnector =>" + "Output from Server .... \n");

            String line = "";
            while ((line = br.readLine()) != null) {
                output = line ;
                System.out.println("DarazConnector =>" + output);
            }

            conn.disconnect();

            JSONObject obj = new JSONObject(output);

            return obj;
        } catch (MalformedURLException e) {

        } catch (IOException e) {

        }

        return null;
    }

    public String getBrands(JSONObject productJson, String store_id)
    {
        System.out.println("DarazConnector =>" + "DarazConnector => getBrands");
        if(productJson.has("brands_use") && productJson.get("brands_use").toString().equals("YES"))
        {
            return productJson.get("brands").toString();
        }
        else if(productJson.has("brands_use") && productJson.get("brands_use").toString().equals("NO"))
        {
            if(!productJson.get("brand").equals(""))
                return productJson.get("brand").toString();
        }

        if(productJson.has("brands"))
        {
            String brand = productJson.get("brands").toString();
            return brand;
        }
        else if(!productJson.get("brand").equals(""))
        {
            return productJson.get("brand").toString();

        }
        return "No Brand";
    }

    public String createProductsXMLNotion(JSONObject productJson, String CatId, String store_id, Users users) throws InterruptedException, IOException, TransformerException, ParserConfigurationException
    {
/*
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

        Document document = documentBuilder.newDocument();

        // Request element
        Element request = document.createElement("Request");
        document.appendChild(request);

        // Product element
        Element product = document.createElement("Product");
        request.appendChild(product);

        // PrimaryCategory element
        Element PrimaryCategory = document.createElement("PrimaryCategory");
        PrimaryCategory.appendChild(document.createTextNode(CatId));
        product.appendChild(PrimaryCategory);

        // SPUId element
        Element SPUId = document.createElement("SPUId");
        product.appendChild(SPUId);

        // AssociatedSku element
        Element AssociatedSku = document.createElement("AssociatedSku");
        product.appendChild(AssociatedSku);

        Element Attributes = document.createElement("Attributes");
//        Attributes.appendChild(document.createTextNode(String.valueOf(attribute)));
        product.appendChild(Attributes);

        // Skus element
        Element Skus = document.createElement("Skus");
//        Skus.appendChild(document.createTextNode(String.valueOf(mergedSKU)));
        product.appendChild(Skus);

        // Attributes element

        JSONObject productAttr = new JSONObject();


        System.out.println("DarazConnector =>" + "DarazConnector => createProducts");
        boolean cat = getCategoryAttributes(CatId, users);

        if(cat) {

            if(productJson.has("product_attributes"))
                productAttr = productJson.getJSONObject("product_attributes");

            HashMap<String, String> dimension = setDimensions(productAttr);

            String xmlBody = "";
            StringBuilder productBody = new StringBuilder();
            StringBuilder mergedSKU = new StringBuilder();
            StringBuilder attribute = new StringBuilder();
            String brands = getBrands(productJson, store_id);

            productJson.put("brand", brands);

            for (int i = 0; i < mandatoryFieldsAttributes.size(); i++) {
                Element att = document.createElement(mandatoryFieldsAttributes.get(i).get("name").toString());

                if (productJson.has(mandatoryFieldsAttributes.get(i).get("name").toString())) {
                    att.appendChild(document.createTextNode(productJson.get(mandatoryFieldsAttributes.get(i).get("name").toString()).toString()));
                }
                else
                {
                    JSONArray array = mandatoryFieldsAttributes.get(i).getJSONArray("options");
                    if(array.length()>0)
                    {
                        JSONObject object = array.getJSONObject(0);
                        att.appendChild(document.createTextNode(object.get("name").toString()));
                    }
                }
                Attributes.appendChild(att);
            }
            for (int i = 0; i < optionalFieldsAttributes.size(); i++) {
                if (productJson.has(optionalFieldsAttributes.get(i).get("name").toString())) {
                    Element optAtt = document.createElement(optionalFieldsAttributes.get(i).get("name").toString());
                    optAtt.appendChild(document.createTextNode(productJson.get(optionalFieldsAttributes.get(i).get("name").toString()).toString()));
                    Attributes.appendChild(optAtt);
                }
            }

            System.out.println("DarazConnector =>" + attribute);

            JSONArray variants = productJson.getJSONArray("variants");

            String weight = "NONE";
            for (int j = 0; j < variants.length(); j++) {

                Element Sku = document.createElement("Sku");
                Skus.appendChild(Sku);

                StringBuilder sku = new StringBuilder();

                JSONObject variantsObj = variants.getJSONObject(j);
                JSONArray optionsValues = new JSONArray();

                if (variantsObj.has("option_values"))
                    optionsValues = variantsObj.getJSONArray("option_values");

                if(weight.equals("NONE"))
                    weight = checkWeight(variantsObj);

                if(dimension.get("package_length") != null)
                {
                    variantsObj.put("package_length", dimension.get("package_length"));
                }
                if(dimension.get("package_width") != null)
                {
                    double w = Double.parseDouble(dimension.get("package_width"));
                    DecimalFormat df = new DecimalFormat("#.#");
                    variantsObj.put("package_width", df.format(w));
                }

                if(!weight.equals("NONE"))
                {
                    double w = Double.parseDouble(weight);
                    DecimalFormat df = new DecimalFormat("#.#");
                    if(df.format(w).equals("0"))
                        w = 0.1;
                    weight = df.format(w);
                }
                variantsObj.put("package_weight", weight);
                variantsObj.put("color_family", setColorFont(optionsValues, productAttr));

                String size = "";
                if(variantsObj.has("default_size"))
                    size = variantsObj.get("default_size").toString();
                boolean diffProducts = false;
                *//*if(!(checkStyleTag(optionsValues)))
                {
                    size = setSize(optionsValues, false, size);
                    String name = productJson.get("name").toString();
                    if(variants.length() == 1) {
                        if(productJson.has("custom_name"))
                            name = getCustomName(name, size);
                    }
                    else
                        name = name.replace("=", "-");

                    Attributes.getAttributeNode("name").setValue(name);

                }
                else
                {
                    diffProducts = true;
                    size = setSize(optionsValues, true, size);
                    String[] temp = size.split("_");
                    if(temp.length >1)
                    {
                        size = temp[0];
                        String name = productJson.get("name").toString();
                        String x = attribute.toString();
                        int firstPos = x.indexOf("<name>") + "<name>".length();
                        int lastPos = x.indexOf("</name>", firstPos);
                        String y = x.substring(0,firstPos) + name + "_" + temp[1] + x.substring(lastPos);
                        System.out.println(y);
                        attribute = new StringBuilder(y);

                    }
                    String short_description = productJson.get("short_description").toString();
                    String x = attribute.toString();
                    int firstPos = x.indexOf("<short_description>") + "<short_description>".length();
                    int lastPos = x.indexOf("</short_description>", firstPos);
                    String y = x.substring(0,firstPos) + short_description + " " + ProjectConstants.refMessage + x.substring(lastPos);
                    System.out.println(y);
                    attribute = new StringBuilder(y);
                }*//*

                if(!size.equals("")) {
                    variantsObj.put("size", size);
                    variantsObj.put("paper_size", size);
                }

                variantsObj.put("price", setPrice(productJson, variantsObj));
                HashMap<String, String> sp_price = setSpecialPrice(variantsObj);
                if(sp_price.size() > 0) {
                    variantsObj.put("special_price", sp_price.get("special_price"));
                    variantsObj.put("special_from_date", sp_price.get("special_from_date"));
                    variantsObj.put("special_to_date", sp_price.get("special_to_date"));
                }
                *//*
                 *   Concat mandatory SKU
                 * *//*

                for (int i = 0; i < mandatoryFieldsSKU.size(); i++) {
                    Element sku1 = document.createElement(mandatoryFieldsSKU.get(i).get("name").toString());

                    if (variantsObj.has(mandatoryFieldsSKU.get(i).get("name").toString())) {
                        sku1.appendChild(document.createTextNode(variantsObj.get(mandatoryFieldsSKU.get(i).get("name").toString()).toString()));
                    }
                    else if(productJson.has(mandatoryFieldsSKU.get(i).get("name").toString()))
                    {
                        sku1.appendChild(document.createTextNode(productJson.get(mandatoryFieldsSKU.get(i).get("name").toString()).toString()));
                    }
                    else
                    {
                        JSONArray array = mandatoryFieldsSKU.get(i).getJSONArray("options");
                        if(array.length()>0)
                        {
                            JSONObject object = array.getJSONObject(array.length()-1);
                            sku1.appendChild(document.createTextNode(object.get("name").toString()));
                        }
                    }
                    Sku.appendChild(sku1);
                }

                for (int i = 0; i < optionalFieldsSKU.size(); i++) {

                    if (variantsObj.has(optionalFieldsSKU.get(i).get("name").toString())) {
                        Element sku1 = document.createElement(optionalFieldsSKU.get(i).get("name").toString() );
                        sku1.appendChild(document.createTextNode(variantsObj.get(optionalFieldsSKU.get(i).get("name").toString()).toString()));
                        Sku.appendChild(sku1);
                    }
                    else if(productJson.has(optionalFieldsSKU.get(i).get("name").toString())) {
                        Element sku1 = document.createElement(optionalFieldsSKU.get(i).get("name").toString() );
                        sku1.appendChild(document.createTextNode(productJson.get(optionalFieldsSKU.get(i).get("name").toString()).toString()));
                        Sku.appendChild(sku1);
                    }
                }

                Element images = document.createElement("Images");
                Sku.appendChild(images);

                ArrayList<String> imagesMap = new ArrayList<>();


                if (variantsObj.has("additional_images")) {
                    JSONArray imagesArray = variantsObj.getJSONArray("additional_images");

                    if (imagesArray != null) {
                        for (int i = 0; i < imagesArray.length(); i++) {
                            JSONObject imageObj = imagesArray.getJSONObject(i);
                            String imageName = imageObj.get("id").toString() + String.valueOf(i);
                            String ImagePath = CustomImageHandler.ImageDownload(imageObj.get("image").toString(), true, imageName);
                            System.out.println("DarazConnector =>" + "DarazConnector => ImagePath-> " + ImagePath);
                            if (!ImagePath.trim().equals("")) {
                                String XMLBody = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                                        "<Request>" +
                                        "    <Image>" +
                                        "        <Url>" + ImagePath + "</Url>" +
                                        "    </Image>" +
                                        "</Request>";

                                String URL = new DarazAPIClass().MigrateImage(users.getUser_id(), users.getApi_key(), XMLBody);
                                System.out.println("DarazConnector =>" + "DarazConnector => uploadImage-> " + URL);

                                if (!URL.trim().equals("")) {
                                    imagesMap.add(URL);
                                }
                            }
                        }
                    }
                } else {
                    if (variantsObj.has("image")) {

                        String ImagePath = CustomImageHandler.ImageDownload(variantsObj.get("image").toString(), true, new Timestamp(System.currentTimeMillis()).toString());
                        System.out.println("DarazConnector =>" + "DarazConnector => ImagePath-> " + ImagePath);
                        if (!ImagePath.trim().equals("")) {
                            String XMLBody = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
                                    "<Request>\n" +
                                    "    <Image>\n" +
                                    "        <Url>" + ImagePath + "</Url>\n" +
                                    "    </Image>\n" +
                                    "</Request>";

                            String URL = new DarazAPIClass().MigrateImage(users.getUser_id(), users.getApi_key(), XMLBody);
                            System.out.println("DarazConnector =>" + "DarazConnector => uploadImage-> " + URL);

                            if (!URL.trim().equals("")) {
                                imagesMap.add(URL);
                            }
                        }
                    }

                }

                if (productJson.has("custom_image")) {
                    imagesMap.add(productJson.get("custom_image").toString());
                }

                imagesMap = removeDuplicateImage(imagesMap);

                for (int i = 0; i < imagesMap.size(); i++) {
                    Element image = document.createElement("Image");
                    image.appendChild(document.createTextNode(imagesMap.get(i)));
                    images.appendChild(image);
                }


                if(diffProducts)
                {
                    DarazProducts darazProducts = new DarazProducts(variantsObj.get("SellerSku").toString(), store_id, CatId, sku.toString(), attribute.toString(), "",
                            new Timestamp(System.currentTimeMillis()), mandatory_attr, mandatory_sku, "1", variantsObj.get("Seller").toString(), variantsObj.get("variant_id").toString()
                            , "", "");
                    darazProductsArrayList.add(darazProducts);
                    createVariants(sku, CatId, attribute, users);

                }
                else {
                    DarazProducts darazProducts = new DarazProducts(variantsObj.get("SellerSku").toString(), store_id, CatId, Sku.toString(), Attributes.toString(), "",
                            new Timestamp(System.currentTimeMillis()), mandatory_attr, mandatory_sku, "1", variantsObj.get("Seller").toString(), variantsObj.get("variant_id").toString()
                            , "", "");
                    darazProductsArrayList.add(darazProducts);
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
//        StreamResult streamResult = new StreamResult(new File(xmlFilePath));

            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            transformer.transform(domSource, result);
            System.out.println(writer.toString());
            xmlBody = writer.toString();
            System.out.println(xmlBody);

            System.out.println("DarazConnector =>" + mergedSKU);

            if (!mergedSKU.toString().trim().equals("")) {

                productBody.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " +
                        "<Request>" +
                        "     <Product>" +
                        "         <PrimaryCategory>" + CatId + "</PrimaryCategory>" +
                        "         <SPUId></SPUId>" +
                        "         <AssociatedSku></AssociatedSku>" +
                        "         <Attributes>" +
                        attribute +
                        "         </Attributes>" +
                        "         <Skus>" +
                        mergedSKU +
                        "         </Skus>" +
                        "     </Product>" +
                        " </Request>");





                // create the xml file
                //transform the DOM Object to an XML File

            }

            System.out.println("DarazConnector =>" + productBody.toString());

            return xmlBody;
        }*/
        return "";
    }

    public String createProducts(JSONObject productJson, String CatId, String store_id, Users users, boolean pushUpdate) throws InterruptedException, IOException, TransformerException, ParserConfigurationException
    {

        sizeObject = getSpecs(store_id+ProjectConstants.darazStoresizeSpecEntityKey);
        if(sizeObject == null)
        {
            sizeObject = getSpecs(ProjectConstants.darazsizeSpecEntityKey);
        }

        JSONObject productAttr = new JSONObject();

        System.out.println("DarazConnector =>" + "DarazConnector => createProducts");
        boolean cat = getCategoryAttributes(CatId, users);

        if(cat) {

            if(productJson.has("product_attributes"))
                productAttr = productJson.getJSONObject("product_attributes");

            HashMap<String, String> dimension = setDimensions(productAttr);

            String xmlBody = "";
            StringBuilder productBody = new StringBuilder();
            StringBuilder mergedSKU = new StringBuilder();
            StringBuilder attribute = new StringBuilder();
            String brands = getBrands(productJson, store_id);

            productJson.put("brand", brands);

            for (int i = 0; i < mandatoryFieldsAttributes.size(); i++) {
                attribute.append("<" + mandatoryFieldsAttributes.get(i).get("name").toString() + ">");
                if (productJson.has(mandatoryFieldsAttributes.get(i).get("name").toString())) {
                    attribute.append(productJson.get(mandatoryFieldsAttributes.get(i).get("name").toString()).toString());
                }
                else
                {
                    JSONArray array = mandatoryFieldsAttributes.get(i).getJSONArray("options");
                    if(array.length()>0)
                    {
                        JSONObject object = array.getJSONObject(0);
                        attribute.append(object.get("name").toString());
                    }
                }
                attribute.append("</" + mandatoryFieldsAttributes.get(i).get("name").toString() + ">");
            }
            for (int i = 0; i < optionalFieldsAttributes.size(); i++) {
                if (productJson.has(optionalFieldsAttributes.get(i).get("name").toString())) {
                    attribute.append("<" + optionalFieldsAttributes.get(i).get("name").toString() + ">");
                    attribute.append(productJson.get(optionalFieldsAttributes.get(i).get("name").toString()).toString());
                    attribute.append("</" + optionalFieldsAttributes.get(i).get("name").toString() + ">");
                }
            }
//            if (productJson.has("short_description_en")) {
//                    attribute.append("<short_description_en>");
//                    attribute.append(productJson.get("short_description_en").toString());
//                    attribute.append("</short_description_en>");
//            }

            System.out.println("DarazConnector =>" + attribute);

            JSONArray variants = productJson.getJSONArray("variants");

            String weight = "NONE";
            for (int j = 0; j < variants.length(); j++) {
                StringBuilder sku = new StringBuilder();

                JSONObject variantsObj = variants.getJSONObject(j);
                JSONArray optionsValues = new JSONArray();

                if (variantsObj.has("option_values"))
                    optionsValues = variantsObj.getJSONArray("option_values");

                if(weight.equals("NONE"))
                    weight = checkWeight(variantsObj);

                if(dimension.get("package_length") != null)
                {
                    variantsObj.put("package_length", dimension.get("package_length"));
                }
                if(dimension.get("package_width") != null)
                {
                    double w = Double.parseDouble(dimension.get("package_width"));
                    DecimalFormat df = new DecimalFormat("#.#");
                    variantsObj.put("package_width", df.format(w));
                }

                if(!weight.equals("NONE"))
                {
                    double w = Double.parseDouble(weight);
                    DecimalFormat df = new DecimalFormat("#.#");
                    if(df.format(w).equals("0"))
                        w = 0.1;
                    weight = df.format(w);
                }
                variantsObj.put("package_weight", weight);
                variantsObj.put("color_family", setColorFont(optionsValues, productAttr));

                String size = "";
                if(variantsObj.has("default_size"))
                    size = variantsObj.get("default_size").toString();
                boolean diffProducts = false;
                if(!(checkStyleTag(optionsValues)))
                {
                    size = setSize(optionsValues, false, size);
                    String name = productJson.get("name").toString();
                    if(variants.length() == 1) {
                        if(productJson.has("custom_name"))
                           name = getCustomName(name, size);
                    }
                    else
                        name = name.replace("=", "-");
                    String x = attribute.toString();
                    int firstPos = x.indexOf("<name>") + "<name>".length();
                    int lastPos = x.indexOf("</name>", firstPos);
                    String y = x.substring(0, firstPos) + name + x.substring(lastPos);
                    System.out.println(y);
                    attribute = new StringBuilder(y);
                }
                else
                {
                    diffProducts = true;
                    size = setSize(optionsValues, true, size);
                    String[] temp = size.split("_");
                    if(temp.length >1)
                    {
                        size = temp[0];
                        String name = productJson.get("name").toString();
                        String x = attribute.toString();
                        int firstPos = x.indexOf("<name>") + "<name>".length();
                        int lastPos = x.indexOf("</name>", firstPos);
                        String y = x.substring(0,firstPos) + name + "_" + temp[1] + x.substring(lastPos);
                        System.out.println(y);
                        attribute = new StringBuilder(y);

                        x = attribute.toString();
                        firstPos = x.indexOf("<name_en>") + "<name_en>".length();
                        lastPos = x.indexOf("</name_en>", firstPos);
                        y = x.substring(0,firstPos) + name + "_" + temp[1] + x.substring(lastPos);
                        System.out.println(y);
                        attribute = new StringBuilder(y);

                    }
                    String short_description = productJson.get("short_description").toString();
                    String x = attribute.toString();
                    int firstPos = x.indexOf("<short_description>") + "<short_description>".length();
                    int lastPos = x.indexOf("</short_description>", firstPos);
                    String y = x.substring(0,firstPos) + short_description + " " + ProjectConstants.refMessage + x.substring(lastPos);
                    System.out.println(y);
                    attribute = new StringBuilder(y);
                }

                if(!size.equals("")) {
                    variantsObj.put("size", size);
                    variantsObj.put("paper_size", size);
                }

                variantsObj.put("price", setPrice(productJson, variantsObj));
                HashMap<String, String> sp_price = setSpecialPrice(variantsObj);
                if(sp_price.size() > 0) {
                    double sp_pr = Double.parseDouble(sp_price.get("special_price"));
                    variantsObj.put("special_price", String.valueOf((int)sp_pr));
                    variantsObj.put("special_from_date", sp_price.get("special_from_date"));
                    variantsObj.put("special_to_date", sp_price.get("special_to_date"));
                }
                /*
                 *   Concat mandatory SKU
                 * */

                sku.append("<Sku>");
                for (int i = 0; i < mandatoryFieldsSKU.size(); i++) {
                    sku.append("<" + mandatoryFieldsSKU.get(i).get("name").toString() + ">");
                    if (variantsObj.has(mandatoryFieldsSKU.get(i).get("name").toString())) {
                        sku.append(variantsObj.get(mandatoryFieldsSKU.get(i).get("name").toString()).toString());
                    }
                    else if(productJson.has(mandatoryFieldsSKU.get(i).get("name").toString()))
                    {
                        sku.append(productJson.get(mandatoryFieldsSKU.get(i).get("name").toString()).toString());
                    }
                    else
                    {
                        JSONArray array = mandatoryFieldsSKU.get(i).getJSONArray("options");
                        if(array.length()>0)
                        {
                            JSONObject object = array.getJSONObject(array.length()-1);
                            sku.append(object.get("name").toString());
                        }
                    }

                    sku.append("</" + mandatoryFieldsSKU.get(i).get("name").toString() + ">");
                }

                for (int i = 0; i < optionalFieldsSKU.size(); i++) {

                    if (variantsObj.has(optionalFieldsSKU.get(i).get("name").toString())) {
                        sku.append("<" + optionalFieldsSKU.get(i).get("name").toString() + ">");
                        sku.append(variantsObj.get(optionalFieldsSKU.get(i).get("name").toString()).toString());
                        sku.append("</" + optionalFieldsSKU.get(i).get("name").toString() + ">");
                    }
                    else if(productJson.has(optionalFieldsSKU.get(i).get("name").toString())) {
                        sku.append("<" + optionalFieldsSKU.get(i).get("name").toString() + ">");
                        sku.append(productJson.get(optionalFieldsSKU.get(i).get("name").toString()).toString());
                        sku.append("</" + optionalFieldsSKU.get(i).get("name").toString() + ">");
                    }
                }
                sku.append("<Images>");

                ArrayList<String> imagesMap = new ArrayList<>();


                if (variantsObj.has("additional_images")) {
                    JSONArray imagesArray = variantsObj.getJSONArray("additional_images");

                    if (imagesArray != null) {
                        for (int i = 0; i < imagesArray.length(); i++) {
                            JSONObject imageObj = imagesArray.getJSONObject(i);
                            String imageName = imageObj.get("id").toString() + String.valueOf(i);
                            String ImagePath = CustomImageHandler.ImageDownload(imageObj.get("image").toString(), true, imageName);
                            System.out.println("DarazConnector =>" + "DarazConnector => ImagePath-> " + ImagePath);
                            if (!ImagePath.trim().equals("")) {
                                String XMLBody = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                                        "<Request>" +
                                        "    <Image>" +
                                        "        <Url>" + ImagePath + "</Url>" +
                                        "    </Image>" +
                                        "</Request>";

                                String URL = new DarazAPIClass().MigrateImage(users.getUser_id(), users.getApi_key(), XMLBody);
                                System.out.println("DarazConnector =>" + "DarazConnector => uploadImage-> " + URL);

                                if (!URL.trim().equals("")) {
                                    imagesMap.add(URL);
                                }
                            }
                        }
                    }
                } else {
                    if (variantsObj.has("image")) {

                        String ImagePath = CustomImageHandler.ImageDownload(variantsObj.get("image").toString(), true, new Timestamp(System.currentTimeMillis()).toString());
                        System.out.println("DarazConnector =>" + "DarazConnector => ImagePath-> " + ImagePath);
                        if (!ImagePath.trim().equals("")) {
                            String XMLBody = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
                                    "<Request>\n" +
                                    "    <Image>\n" +
                                    "        <Url>" + ImagePath + "</Url>\n" +
                                    "    </Image>\n" +
                                    "</Request>";

                            String URL = new DarazAPIClass().MigrateImage(users.getUser_id(), users.getApi_key(), XMLBody);
                            System.out.println("DarazConnector =>" + "DarazConnector => uploadImage-> " + URL);

                            if (!URL.trim().equals("")) {
                                imagesMap.add(URL);
                            }
                        }
                    }

                }

                if (productJson.has("custom_image")) {
                    imagesMap.add(productJson.get("custom_image").toString());
                }

                imagesMap = removeDuplicateImage(imagesMap);

                for (int i = 0; i < imagesMap.size(); i++) {
                    sku.append("<Image>");
                    sku.append(imagesMap.get(i));
                    sku.append("</Image>");
                }

                sku.append("</Images>");

                String status = "";
                if(productJson.has("status") && productJson.get("status").toString().equals("1.0")) {
                    sku.append("<Status>active</Status>");
                    status = "1";
                }
                else {
                    status = "0";
                    sku.append("<Status>inactive</Status>");
                }
                sku.append("</Sku>");

                if(diffProducts)
                {
                    DarazProducts darazProducts = new DarazProducts(variantsObj.get("SellerSku").toString(), store_id, CatId, sku.toString(), attribute.toString(), "",
                            new Timestamp(System.currentTimeMillis()), mandatory_attr, mandatory_sku, "1", variantsObj.get("Seller").toString(), variantsObj.get("variant_id").toString()
                            , "", "", "1");
                    darazProductsArrayList.add(darazProducts);
                    createVariants(sku, CatId, attribute, users, pushUpdate, status);

                }
                else {
                    mergedSKU.append(sku);
                    DarazProducts darazProducts = new DarazProducts(variantsObj.get("SellerSku").toString(), store_id, CatId, sku.toString(), attribute.toString(), "",
                            new Timestamp(System.currentTimeMillis()), mandatory_attr, mandatory_sku, "1", variantsObj.get("Seller").toString(), variantsObj.get("variant_id").toString()
                            , "", "", "1");
                    darazProductsArrayList.add(darazProducts);
                }
            }

            System.out.println("DarazConnector =>" + mergedSKU);

            if (!mergedSKU.toString().trim().equals("")) {

                productBody.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " +
                        "<Request>" +
                        "     <Product>" +
                        "         <PrimaryCategory>" + CatId + "</PrimaryCategory>" +
                        "         <SPUId></SPUId>" +
                        "         <AssociatedSku></AssociatedSku>" +
                        "         <Attributes>" +
                        attribute +
                        "         </Attributes>" +
                        "         <Skus>" +
                        mergedSKU +
                        "         </Skus>" +
                        "     </Product>" +
                        " </Request>");


                /*DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

                DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

                Document document = documentBuilder.newDocument();

                // Request element
                Element request = document.createElement("Request");
                document.appendChild(request);

                // Product element
                Element product = document.createElement("Product");
                request.appendChild(product);

                // PrimaryCategory element
                Element PrimaryCategory = document.createElement("PrimaryCategory");
                PrimaryCategory.appendChild(document.createTextNode(CatId));
                product.appendChild(PrimaryCategory);

                // SPUId element
                Element SPUId = document.createElement("SPUId");
                product.appendChild(SPUId);

                // AssociatedSku element
                Element AssociatedSku = document.createElement("AssociatedSku");
                product.appendChild(AssociatedSku);

                // Attributes element
                Element Attributes = document.createElement("Attributes");
                Attributes.appendChild(document.createTextNode(String.valueOf(attribute)));
                product.appendChild(Attributes);

                // Skus element
                Element Skus = document.createElement("Skus");
                Skus.appendChild(document.createTextNode(String.valueOf(mergedSKU)));
                product.appendChild(Skus);

                // create the xml file
                //transform the DOM Object to an XML File
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource domSource = new DOMSource(document);
//        StreamResult streamResult = new StreamResult(new File(xmlFilePath));

                StringWriter writer = new StringWriter();
                StreamResult result = new StreamResult(writer);
                transformer.transform(domSource, result);
                System.out.println(writer.toString());
                xmlBody = writer.toString();
                System.out.println(xmlBody);*/
            }

            System.out.println("DarazConnector =>" + productBody.toString());

            return productBody.toString();
        }
        return "";
    }


    public String getCustomName(String name, String size)
    {
        String concatName = "";
        String nameSplit[] = name.split("=") ;
        for(int i=0; i<nameSplit.length ;i++) {
            if (i != nameSplit.length-1)
                concatName += nameSplit[i] + " - ";
            else {
                concatName += size + " - ";
            }
        }
        concatName += nameSplit[nameSplit.length-1];

        return concatName;
    }

    public String createVariants(StringBuilder sku, String CatId, StringBuilder attribute, Users users, boolean pushUpdate, String active_status) throws IOException {
        StringBuilder productBody = new StringBuilder();
        productBody.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " +
                "<Request>" +
                "     <Product>" +
                "         <PrimaryCategory>" + CatId + "</PrimaryCategory>" +
                "         <SPUId></SPUId>" +
                "         <AssociatedSku></AssociatedSku>" +
                "         <Attributes>" +
                attribute +
                "         </Attributes>" +
                "         <Skus>" +
                sku +
                "         </Skus>" +
                "     </Product>" +
                " </Request>");


        System.out.println("DarazConnector => createProductRequest-> " + productBody.toString());

//        String response = darazAPIClass.UpdateProduct(users.getUser_id(), users.getApi_key(), productBody.toString());
//
//        if(!response.contains("SuccessResponse"))
//        {
//          response = darazAPIClass.CreateProduct1(users.getUser_id(), users.getApi_key(), productBody.toString());
//        }

        String response = "";
        if(pushUpdate)
        {
            response = darazAPIClass.UpdateProduct(users.getUser_id(), users.getApi_key(), productBody.toString());
            JSONObject respObject = new JSONObject(response);
            if (respObject.has("SuccessResponse")) {
                SetDarazProductsCassandra_Logs("1", productBody.toString(), response,active_status);
            } else {
                SetDarazProductsCassandra_Logs("0", productBody.toString(), response,active_status);
            }
        }
        else {
            response = darazAPIClass.CreateProduct1(users.getUser_id(), users.getApi_key(), productBody.toString());

//            response = darazAPIClass.UpdateProduct(users.getUser_id(), users.getApi_key(), productBody.toString());
//            if(!response.contains("Success"))
//                response = darazAPIClass.CreateProduct1(users.getUser_id(), users.getApi_key(), productBody.toString());
//
            JSONObject respObject = new JSONObject(response);
            if (respObject.has("SuccessResponse")) {
                SetDarazProductsCassandra_Logs("1", productBody.toString(), response,"1");
            } else {
                SetDarazProductsCassandra_Logs("0", productBody.toString(), response,"0");
            }
        }
        System.out.println("DarazConnector => createProductResponse-> " + response);



        return response;

    }

    public boolean checkStyleTag(JSONArray jsonArray)
    {
        for(int i=0; i<jsonArray.length(); i++)
        {
            JSONObject obj = jsonArray.getJSONObject(i);
            if(obj.has("option_id") && obj.get("option_id").toString().equals("400001"))
            {
                return true;
            }
        }

        return false;
    }

    public ArrayList<String> removeDuplicateImage(ArrayList<String> imagesMap)
    {
        for(int i=0; i<imagesMap.size()-1; i++)
        {
//            Collections.sort(imagesMap);
            if(imagesMap.get(i).equals(imagesMap.get(i+1)))
            {
                imagesMap.remove(i+1);
                i--;
            }
        }

        return imagesMap;
    }

    private String getPrice(JSONObject variantObj, String srcCurrency, String destCurrency)
    {
        Double rate = getConversionRate(srcCurrency, destCurrency);
        Double price = 0.0;

        if(variantObj.has("price")) {
            price = Double.parseDouble(variantObj.get("price").toString());
            price = price * rate;
        }
        int roundPrice= (int)Math.round(price);
        return roundPrice+"";
    }

    public Double getConversionRate(String srcCurrency, String destCurrency)
    {
        JSONObject obj = getSpecs(ProjectConstants.currencyConversionEntityKey);
        Double rate = 0.0;

        Double src = Double.parseDouble(obj.get(srcCurrency).toString());
        Double dest = Double.parseDouble(obj.get(destCurrency).toString());
        rate = dest/src;
        return rate;
    }

    private String checkWeight(JSONObject variantObj)
    {
        if(variantObj.get("package_weight").toString().contains("E")) {
            String weight = variantObj.get("package_weight").toString();
            double we = Double.parseDouble(weight);
            DecimalFormat formatter = new DecimalFormat("0.0000");
            System.out.println("DarazConnector =>" + formatter.format(we));
            return formatter.format(we);
        }
        return variantObj.get("package_weight").toString();
    }

    private HashMap<String, String> setDimensions(JSONObject productAttr)
    {
        HashMap<String, String> dimens = new HashMap<>();
        if (productAttr != null)
        {
            if(productAttr.has("9996")) {
                String dim = productAttr.get("9996").toString();
                String[] splitDim = dim.split("x");
                for (int i = 0; i < splitDim.length; i++) {
                    if (splitDim[i].contains("L")) {
                        splitDim[i] = splitDim[i].replace("L ", "");
                        dimens.put("package_length", splitDim[i].trim());
                    } else if (splitDim[i].contains("W")) {
                        splitDim[i] = splitDim[i].replace("W ", "");
                        dimens.put("package_width", splitDim[i].trim());
                    }
                }
            }
            else if(productAttr.has("99912"))
            {
                dimens.put("package_width", productAttr.getString("99912"));
            }
            else if(productAttr.has("99914"))
            {
                dimens.put("package_height", productAttr.getString("99914"));
            }
        }
        return dimens;
    }

    private String setColorFont(JSONArray optionValues, JSONObject productAttr)
    {
        String color = "...";
        for(int i=0; i<optionValues.length(); i++)
        {
            if(optionValues.getJSONObject(i).get("option_name").toString().equals("color") || optionValues.getJSONObject(i).get("option_name").toString().toLowerCase().equals("colour"))
            {
                color = optionValues.getJSONObject(i).get("option_value_name").toString();
            }
        }

        if(productAttr != null)
        {
            if(productAttr.has("9991"))
            {
                color = productAttr.get("9991").toString();
            }
        }

        return color;
        /*if(colorObject.has(color))
        {
            return color;
        }
        else
        {
            return color;
            *//*String[] colorSp = color.split(" ");
            for(int i=0; i<colorSp.length; i++)
            {
                if(colorObject.has(colorSp[i]))
                {
                    return colorSp[i];
                }
            }*//*
        }*/

        //return "...";
    }

    private String setPrice(JSONObject productJson, JSONObject variantsObj)
    {
        if(productJson.has("custom_price_percent")) {
            double percent = Double.parseDouble(productJson.get("custom_price_percent").toString());
            double price = 0;
            if(variantsObj.has("price"))
            {
                price = Double.parseDouble(variantsObj.get("price").toString());
            }
            else
            {
                price = Double.parseDouble(productJson.get("price").toString());
            }
            double total_price = (price/100)*percent;
            total_price += price;
            return String.valueOf((int)total_price);

        }

        if(variantsObj.has("price"))
        {
            return variantsObj.get("price").toString();
        }
        return productJson.get("price").toString();
    }

    private HashMap<String, String> setSpecialPrice(JSONObject variantsObj)
    {
        HashMap<String, String> sp_Price = new HashMap<>();
        if(variantsObj.has("custom_sale"))
        {
            if(variantsObj.has("price"))
            {
                double price = Double.parseDouble(variantsObj.get("price").toString());
                double percent = Double.parseDouble(variantsObj.get("custom_sale").toString());
                double total_price = (price/100)*percent;
                price -= total_price;
                sp_Price.put("special_price", String.valueOf((int)price));

                String pattern = "yyyy-MM-dd";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

                Date fromDate = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(fromDate);
                c.add(Calendar.WEEK_OF_MONTH, 2);
                Date toDate = c.getTime();

                String fromDate1 = simpleDateFormat.format(fromDate);
                String toDate1 = simpleDateFormat.format(toDate);
                sp_Price.put("special_from_date", fromDate1);
                sp_Price.put("special_to_date", toDate1);
            }
        }
        else if(variantsObj.has("product_specials")) {
            JSONArray jsonArray = variantsObj.getJSONArray("product_specials");
            if (jsonArray.length() > 0) {
                JSONObject specialObject = jsonArray.getJSONObject(0);
                if (specialObject.has("special_price")) {
                    sp_Price.put("special_price", specialObject.get("special_price").toString());

                    String pattern = "yyyy-MM-dd";
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

                    Date fromDate = new Date();
                    Calendar c = Calendar.getInstance();
                    c.setTime(fromDate);
                    c.add(Calendar.WEEK_OF_MONTH, 2);
                    Date toDate = c.getTime();

                    String fromDate1 = simpleDateFormat.format(fromDate);
                    String toDate1 = simpleDateFormat.format(toDate);

                    if (specialObject.get("special_from_date").toString().contains("0000") || specialObject.get("special_from_date").toString().equals("")) {
                        sp_Price.put("special_from_date", fromDate1);
                        sp_Price.put("special_to_date", toDate1);
                    } else {
                        sp_Price.put("special_from_date", specialObject.get("special_from_date").toString());
                        sp_Price.put("special_to_date", specialObject.get("special_to_date").toString());
                    }
                }
            }
        }

        return sp_Price;
    }

    private String setSize(JSONArray optionValues, boolean style, String default_size)
    {
        for(int i=0; i<optionValues.length(); i++)
        {
            if(!style) {
                if (optionValues.getJSONObject(i).get("option_name").toString().equals("size")) {
                    String size = optionValues.getJSONObject(i).get("option_value_name").toString();
                    if (sizeObject.has(size))
                        return sizeObject.get(size).toString();
                }
            }
            else
            {
                if (optionValues.getJSONObject(i).get("option_id").toString().equals("400001")) {
                    String size = optionValues.getJSONObject(i).get("option_value_name").toString();
                    if (sizeObject.has(size)) {
                        return sizeObject.get(size).toString() + "_" + size;
                    }
                }
            }
        }

        for(int i=0; i<mandatoryFieldsSKU.size(); i++)
        {
            String size = "";
            String fieldName = mandatoryFieldsSKU.get(i).get("name").toString();
            if(fieldName.contains("size"))
            {
                JSONArray sizeArr = mandatoryFieldsSKU.get(i).getJSONArray("options");
                size = getDefaultSizes(sizeArr, default_size);
                if(!size.equals(""))
                {
                    return size;
                }
            }
        }

        for(int i=0; i<optionalFieldsSKU.size(); i++)
        {
            String size = "";
            String fieldName = optionalFieldsSKU.get(i).get("name").toString();
            if(fieldName.contains("size"))
            {
                JSONArray sizeArr = optionalFieldsSKU.get(i).getJSONArray("options");
                size = getDefaultSizes(sizeArr, default_size);
                if(!size.equals(""))
                {
                    return size;
                }
            }
        }

        return "";
    }

    private String getDefaultSizes(JSONArray sizeArr, String def_Size)
    {
        for(int j=0; j<sizeArr.length(); j++)
        {
            JSONObject object = sizeArr.getJSONObject(j);
            String sizes =  object.get("name").toString();
            if(sizes.contains(def_Size))
            {
                return def_Size;
            }
        }

        for(int j=0; j<sizeArr.length(); j++)
        {
            JSONObject object = sizeArr.getJSONObject(j);
            String sizes =  object.get("name").toString();
            if(sizes.contains("Un-stitched"))
            {
                return "Un-stitched";
            }
            else if(sizes.contains("..."))
            {
                return "...";
            }
            else if(sizes.contains("Not Specified"))
            {
                return "Not Specified";
            }
            else if(sizes.contains("Int: One size"))
            {
                return "Int: One size";
            }
            else if(sizes.contains("N/A"))
            {
                return "N/A";
            }
            else if(sizes.contains("Adjustable"))
            {
                return "Adjustable";
            }
        }
        return "";
    }
}
