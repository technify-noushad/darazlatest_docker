package com.example.DarazConsumerProducer.Main;

import com.example.DarazConsumerProducer.Constants.ProjectConstants;
import com.example.DarazConsumerProducer.DarazSDK.DarazAPIClass;
import com.example.DarazConsumerProducer.DarazSDK.SellercenterAPI;
import com.example.DarazConsumerProducer.HelperClasses.CustomImageHandler;
import com.example.DarazConsumerProducer.HelperClasses.ElasticSearchCalls;
import com.example.DarazConsumerProducer.HelperClasses.HelpingUtilities;
import com.example.DarazConsumerProducer.cassandra.models.DarazProducts;
import com.example.DarazConsumerProducer.cassandra.models.DarazPushUpdates;
import com.example.DarazConsumerProducer.cassandra.models.DarazPushUpdates_Initial_Logs;
import com.example.DarazConsumerProducer.cassandra.models.Users;
import com.example.DarazConsumerProducer.cassandra.services.interfaces.*;
import com.example.DarazConsumerProducer.configurations.MainAppConfiguration;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

public class MainApplicationClass {

    private static ApplicationContext applicationContext =
            new AnnotationConfigApplicationContext(MainAppConfiguration.class);
    static UsersService usersService = applicationContext.getBean(UsersService.class);
    static DarazProductsService darazProductsService = applicationContext.getBean(DarazProductsService.class);
    static DarazPushUpdatesService darazPushUpdatesService = applicationContext.getBean(DarazPushUpdatesService.class);
    static DarazPushUpdates_InitialLogsService darazPushUpdates_initialLogsService = applicationContext.getBean(DarazPushUpdates_InitialLogsService.class);
    SellercenterAPI sellercenterAPI = new SellercenterAPI();

    ElasticSearchCalls elasticSearchCalls = new ElasticSearchCalls();


    /*public static void main(String[] args) throws IOException, TransformerException, InterruptedException {

//        Thread[] threads = new Thread[50];
//        List<DarazProducts> darazProducts = darazProductsService.getAllProducts();
//        for (int i=0;i<darazProducts.size(); i++)
//        {
//            for(int j=0; j<50; j++) {
//                if(i<darazProducts.size()) {
//                    DarazProducts darazProducts1 = darazProducts.get(i++);
//
//                    threads[j] = new Thread(new Runnable() {
//                        public void run() {
//                            darazProducts1.setIs_active(darazProducts1.getCreate_status());
//                            darazProductsService.createProducts(darazProducts1);
//                        }
//                    });
//                    threads[j].start();
//                }
//            }
//
//            for (int j = 0; j < threads.length; j++)
//                threads[j].join();
//
//            i--;
//        }



//        try {
//            new MainApplicationClass().formXML();
//        } catch (ParserConfigurationException e) {
//            e.printStackTrace();
//        }
//        new MainApplicationClass().testSingleProduct("a6b5bba11b114a81bac5e3dd7e839eec");
//
//        new MainApplicationClass().setMissingImages(sku, "05570acb83f54ba4bf60902b7a169f63");
//        new MainApplicationClass().uploadProducts(sku, "a6b5bba11b114a81bac5e3dd7e839eec");

//        final String out = new DarazAPIClass().GetCategoryTree(UserID, apiKey, "");
//        JSONObject jsonObject = new JSONObject(out);
//
//        System.out.println("DarazConnector =>" + out); // print out the XML response

//        new MainApplicationClass().MigrateErrorProducts("100011", sku);

         LinkedHashMap<String, Object> jsonDetails = new Gson().fromJson(value, new TypeToken<LinkedHashMap<String, Object>>() {
//        }.getType());
//
//        new WorkingClass().pushUpdates(jsonDetails);
//
// try {
//            new MainApplicationClass().connectorMainFunction("a6b5bba11b114a81bac5e3dd7e839eec");
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//            System.out.println("DarazConnector =>" + HelpingUtilities.StackTraceToString(e));
//        }


//        final String out = new DarazAPIClass().UpdatePriceQuantity(UserID, apiKey, XML);
//        final String out = new DarazAPIClass().GetCategoryAttributes(UserID, apiKey, "", "10002350");
//        final String out = new DarazAPIClass().CreateProduct(UserID, apiKey,XML);

    }
*/

    public void uploadProducts(ArrayList<String> skus, String store_id) throws IOException
    {
        Users users =  usersService.findByStoreId(store_id);
        JSONObject obj = new WorkingClass().getSpecs(store_id+ProjectConstants.categoryMappingEntity);

        for(int i=0; i<skus.size(); i++) {

            String sku = skus.get(i);
            String[] temp = sku.split("_");
            List<LinkedHashMap<String, Object>> linkedHashMaps = elasticSearchCalls
                    .fetchFromElasticBySKUs(ElasticSearchCalls.ElasticIndex.REPLICATE_CATALOG,
                            ElasticSearchCalls.ElasticType.PRODUCT,
                            store_id, temp[0], temp[1].replace("T",""), 20);

            ArrayList<String> imagesMap = new ArrayList<>();

            for (int j = 0; j < linkedHashMaps.size(); j++) {
                LinkedHashMap<String, Object> productMap = linkedHashMaps.get(j);

                try {
                    new UpdateProductsClass().doWork(productMap, store_id, users, obj);
                } catch (Exception ex) {
                    System.out.println("DarazConnector => Exception" + ex.getMessage());
                }
            }
        }
    }

    public void setMissingImages(ArrayList<String> skus, String store_id) throws IOException {

        Users users =  usersService.findByStoreId(store_id);

        for(int i=0; i<skus.size(); i++)
        {
            String setImages = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Request><Product><Skus><Sku>";
            setImages +="<SellerSku>" + skus.get(i) + "</SellerSku>";
            setImages +="<Images>";

            String sku = skus.get(i);
            String[] temp = sku.split("_");
            List<LinkedHashMap<String, Object>> linkedHashMaps = elasticSearchCalls
                    .fetchFromElasticBySKUs(ElasticSearchCalls.ElasticIndex.REPLICATE_CATALOG,
                            ElasticSearchCalls.ElasticType.PRODUCT,
                            store_id, temp[0] , temp[1], 20);

            ArrayList<String> imagesMap = new ArrayList<>();

            for(int j=0; j<linkedHashMaps.size(); j++)
            {
                LinkedHashMap<String, Object> productMap = linkedHashMaps.get(j);
                JSONObject object = new JSONObject(productMap);
                JSONArray variants = object.getJSONArray("variants");
                JSONObject variantsObj = variants.getJSONObject(0);


                if (variantsObj.has("additional_images")) {
                    JSONArray imagesArray = variantsObj.getJSONArray("additional_images");

                    if (imagesArray != null) {
                        for (int k = 0; k < imagesArray.length(); k++) {
                            JSONObject imageObj = imagesArray.getJSONObject(k);
                            String imageName = imageObj.get("id").toString()+ String.valueOf(k);
                            String ImagePath = CustomImageHandler.ImageDownload(imageObj.get("image").toString(), true, imageName);
                            System.out.println("DarazConnector =>" + "DarazConnector => ImagePath-> " + ImagePath);
                            if(!ImagePath.trim().equals("")) {
                                String XMLBody = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                                        "<Request>" +
                                        "    <Image>" +
                                        "        <Url>" + ImagePath + "</Url>" +
                                        "    </Image>" +
                                        "</Request>";

                                String URL = new DarazAPIClass().MigrateImage(users.getUser_id(), users.getApi_key(), XMLBody);
                                System.out.println("DarazConnector =>" + "DarazConnector => uploadImage-> " + URL);

                                if (!URL.trim().equals("")) {
                                    imagesMap.add(URL);
                                }
                            }
                        }
                    }
                } else {
                    if (variantsObj.has("image")) {

                        String ImagePath = CustomImageHandler.ImageDownload(variantsObj.get("image").toString(), true, new Timestamp(System.currentTimeMillis()).toString());
                        System.out.println("DarazConnector =>" + "DarazConnector => ImagePath-> " + ImagePath);
                        if(!ImagePath.trim().equals("")) {
                            String XMLBody = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
                                    "<Request>\n" +
                                    "    <Image>\n" +
                                    "        <Url>" + ImagePath + "</Url>\n" +
                                    "    </Image>\n" +
                                    "</Request>";

                            String URL = new DarazAPIClass().MigrateImage(users.getUser_id(), users.getApi_key(), XMLBody);
                            System.out.println("DarazConnector =>" + "DarazConnector => uploadImage-> " + URL);

                            if (!URL.trim().equals("")) {
                                imagesMap.add(URL);
                            }
                        }
                    }

                }
            }

            for(int j=0; j<imagesMap.size(); j++)
            {
                setImages += "<Image>";
                setImages += imagesMap.get(j);
                setImages += "</Image>";
            }

            setImages += "</Images>";
            setImages += "</Sku></Skus></Product></Request>";

            String url = new DarazAPIClass().SetImages(users.getUser_id(), users.getApi_key(), setImages);
            System.out.println(url);

        }




    }

    public void MigrateErrorProducts(String store_id, ArrayList<String> sku) throws TransformerException, IOException {
        Users users =  usersService.findByStoreId(store_id);

        if(users != null) {
            for(int i=0; i<sku.size(); i++) {
                DarazProducts darazProducts = darazProductsService.findBydaraz_Sku_id(sku.get(i), store_id);

                    if (darazProducts != null && darazProducts.getCreate_status().equals("0")) {
                        String XML = darazProducts.getRequest_xml().replace("&nbsp;", "");
                        XML = XML.replace(" & ", "&#38;");
                        //new MainApplicationClass().format(darazProducts.getRequest_xml());
                        System.out.println("DarazConnector =>" + darazProducts.getRequest_xml());

                        String response = new DarazAPIClass().CreateProduct1(users.getUser_id(), users.getApi_key(), XML);
                        if (response.contains("SuccessResponse")) {
                            darazProducts.setResponse(response);
                            darazProducts.setCreate_status("1");

                            darazProductsService.createProducts(darazProducts);
                        }

                }
            }

        }
    }

    public String format(String unformattedXml) {
        try {
            final Document document = parseXmlFile(unformattedXml);

            OutputFormat format = new OutputFormat(document);
            format.setLineWidth(65);
            format.setIndenting(true);
            format.setIndent(2);
            Writer out = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(out, format);
            serializer.serialize(document);

            return out.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Document parseXmlFile(String in) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(in));
            return db.parse(is);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (org.xml.sax.SAXException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void testSingleProduct(String store_id) throws IOException {
        Users users =  usersService.findByStoreId(store_id);

        List<LinkedHashMap<String, Object>> linkedHashMaps = elasticSearchCalls
                .fetchFromElasticBySKUs(ElasticSearchCalls.ElasticIndex.REPLICATE_CATALOG,
                        ElasticSearchCalls.ElasticType.PRODUCT,
                        store_id, "201806", "", 20);

        JSONObject obj = new WorkingClass().getSpecs(store_id+ProjectConstants.categoryMappingEntity);

        for (int j = 0; j < linkedHashMaps.size(); j++) {
            LinkedHashMap<String, Object> productMap = linkedHashMaps.get(j);

            try {
                new WorkingClass().doWork(productMap, store_id, users, obj);
            } catch (Exception ex) {
                System.out.println("DarazConnector => Exception" + ex.getMessage());
            }
        }

    }

    public void connectorMainFunction(String store_id, String status_migration) throws InterruptedException {

        System.out.println("DarazConnector =>" + "DarazConnector => connectorMainFunction");
        Users users =  usersService.findByStoreId(store_id);


        if(users != null) {
            System.out.println("DarazConnector =>" + users.toString());

            List<LinkedHashMap<String, Object>> linkedHashMaps = elasticSearchCalls
                    .fetchFromElasticByStoreID(ElasticSearchCalls.ElasticIndex.REPLICATE_CATALOG,
                            ElasticSearchCalls.ElasticType.PRODUCT,
                            store_id, 40);

            int zeroCount = 0;
            Thread[] threads = new Thread[2];
            JSONObject obj = new WorkingClass().getSpecs(store_id+ProjectConstants.categoryMappingEntity);

            do {
                System.out.println("DarazConnector =>" + "DarazConnector");
                for (int i = 0; i < linkedHashMaps.size(); i++) {
                    LinkedHashMap<String, Object> productMap = linkedHashMaps.get(i);

                    JSONObject object = new JSONObject(productMap);
                    System.out.println("DarazConnector =>" + object);
                    String status = productMap.get("status").toString();
                    String quan = productMap.get("quantity").toString();
                    if (!status.equals("0")){
                        try {
                            if(status_migration.equals("new_migration"))
                                new WorkingClass().doWork(productMap, store_id, users, obj);
                            else if(status_migration.equals("update_migration"))
                                new UpdateProductsClass().doWork(productMap, store_id, users, obj);

                        }
                        catch (Exception ex)
                        {
                            System.out.println("DarazConnector => Exception" + ex.getMessage());
                        }
                    }
                    else
                    {
                        zeroCount++;
                    }

                    /*threads[i] = new Thread(new Runnable() {
                        public void run() {
                            System.out.println("DarazConnector =>" + "Thread Started");
                            JSONObject object = new JSONObject(productMap);
                            System.out.println("DarazConnector =>" + object);
                            String status = productMap.get("status").toString();
                            if (!status.equals("0")){
                                try {
                                    new WorkingClass().doWork(productMap, store_id, users, obj);
                                }
                                catch (Exception ex)
                                {
                                    System.out.println("DarazConnector => Exception" + ex.getMessage());
                                }
                            }
                        }
                    });
                    threads[i].start();*/
                }

                /*System.out.println("DarazConnector =>" + "waiting for threads to finish");
                for (int i = 0; i < threads.length; i++)
                    threads[i].join();*/

            } while ((linkedHashMaps = elasticSearchCalls.fetchNext(40)) != null);


            System.out.println("DarazConnector =>" + "DONE\n\n\n\n");
            System.out.println("DarazConnector =>" + "MIGRATION COMPLETED\n\n");
            System.out.println("ZERO Status " + zeroCount);
        }
    }

    public void schedularForErrorResponsePushUpdates()
    {
        List<DarazPushUpdates_Initial_Logs> darazPushUpdates_initial_logs = darazPushUpdates_initialLogsService.findByStatus("0");
        for(int i=0; i<darazPushUpdates_initial_logs.size(); i++)
        {
            Users users =  usersService.findByStoreId(darazPushUpdates_initial_logs.get(i).getStore_id());
            if(users != null) {
                DarazPushUpdates_Initial_Logs darazPushUpdates_initial_logs1 = darazPushUpdates_initial_logs.get(i);
                String XML = darazPushUpdates_initial_logs.get(i).getRequest_xml();
                String response = new DarazAPIClass().UpdatePriceQuantity(users.getUser_id(), users.getApi_key(), XML);
                darazPushUpdates_initial_logs1.setStatus("1");
                darazPushUpdates_initial_logs1.setResponse(response);
                darazPushUpdates_initial_logs1.setUpdated_at(new Timestamp(System.currentTimeMillis()));
                darazPushUpdates_initialLogsService.createProducts(darazPushUpdates_initial_logs1);
                DarazPushUpdates darazPushUpdates = new DarazPushUpdates(generateUUID(),darazPushUpdates_initial_logs1.getStore_id(), darazPushUpdates_initial_logs1.getDaraz_sku_id(), darazPushUpdates_initial_logs1.getRequest_xml(), darazPushUpdates_initial_logs1.getResponse(), darazPushUpdates_initial_logs1.getUpdated_at(), darazPushUpdates_initial_logs1.getVariant_id(), darazPushUpdates_initial_logs1.getTechnify_id());
                darazPushUpdatesService.createProducts(darazPushUpdates);

            }
        }
     }

    private String generateUUID()
    {
        long msb = System.currentTimeMillis();
        long lsb = System.currentTimeMillis();
        //initialize uuid
        UUID uuid = new UUID(msb, lsb);
        return uuid.toString();
    }
}