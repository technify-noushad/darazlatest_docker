package com.example.DarazConsumerProducer.Logs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by farhan on 07/05/2018.
 */
public abstract class L {

    private static Map<String, String> map = new HashMap<>();


    // ERROR

    public static void e(String devName, String projectName, String className, String methodName, String description) {
        e(devName, projectName, className, methodName, description, "N/A", "ERROR", "HIGH");
    }
    public static void e(String devName, String projectName, String className, String methodName, String description, String stackTrace) {
        e(devName, projectName, className, methodName, description, stackTrace, "ERROR", "HIGH");
    }
    public static void e(String devName, String projectName, String className, String methodName, String description, String stackTrace, String logType, String priority) {
        map = MapFields(devName, projectName, className, methodName, description, stackTrace, logType, priority);
        printAndSendLogs(map);
    }



    // WARNING

    public static void w(String devName, String projectName, String className, String methodName, String description) {
        w(devName, projectName, className, methodName, description, "N/A", "WARN", "MEDIUM");
    }
    public static void w(String devName, String projectName, String className, String methodName, String description, String stackTrace) {
        w(devName, projectName, className, methodName, description, stackTrace, "WARN", "MEDIUM");
    }
    public static void w(String devName, String projectName, String className, String methodName, String description, String stackTrace, String logType, String priority) {
        map = MapFields(devName, projectName, className, methodName, description, stackTrace, logType, priority);
        printAndSendLogs(map);
    }


    // INFO

    public static void i(String devName, String projectName, String className, String methodName, String description) {
        i(devName, projectName, className, methodName, description, "N/A", "INFO", "NONE");
    }
    public static void i(String devName, String projectName, String className, String methodName, String description, String stackTrace) {
        i(devName, projectName, className, methodName, description, stackTrace, "INFO", "NONE");
    }
    public static void i(String devName, String projectName, String className, String methodName, String description, String stackTrace, String logType, String priority) {
        map = MapFields(devName, projectName, className, methodName, description, stackTrace, logType, priority);
        printAndSendLogs(map);
    }


    // DEBUG

    public static void d(String devName, String projectName, String className, String methodName, String description) {
        d(devName, projectName, className, methodName, description, "N/A", "DEBUG", "NONE");
    }
    public static void d(String devName, String projectName, String className, String methodName, String description, String stackTrace) {
        d(devName, projectName, className, methodName, description, stackTrace, "DEBUG", "NONE");
    }
    public static void d(String devName, String projectName, String className, String methodName, String description, String stackTrace, String logType, String priority) {
        map = MapFields(devName, projectName, className, methodName, description, stackTrace, logType, priority);
        printAndSendLogs(map);
    }


    // SUCCESS

    public static void s(String devName, String projectName, String className, String methodName) {
        s(devName, projectName, className, methodName, "Code executed successfully", "N/A", "SUCCESS", "NONE");
    }
    public static void s(String devName, String projectName, String className, String methodName, String description, String stackTrace) {
        s(devName, projectName, className, methodName, description, stackTrace, "SUCCESS", "NONE");
    }
    private static void s(String devName, String projectName, String className, String methodName, String description, String stackTrace, String logType, String priority) {
        map = MapFields(devName, projectName, className, methodName, description, stackTrace, logType, priority);
        printAndSendLogs(map);
    }



    // Helper methods

    private static Map<String, String> MapFields(String devName, String projectName, String className, String methodName, String description, String stackTrace, String logType, String priority) {
        LinkedHashMap<String, String> tempMap = new LinkedHashMap<>();
        tempMap.put("Logger name", devName);
        tempMap.put("Project name", projectName);
        tempMap.put("Class name", className);
        tempMap.put("Method name", methodName);
        tempMap.put("Description", description);
        tempMap.put("Stack trace", stackTrace);
        tempMap.put("Date & time", generateTimeStamp());
        tempMap.put("Log type", logType);
        tempMap.put("Priority", priority);

        return tempMap;
    }
    private static void printLog(Map<String, String> m) {
        for(Map.Entry entry : m.entrySet())
            System.out.println("Logs: " + entry.getKey() + ": " + entry.getValue());
    }
    private static void pushToTopic(Map<String, String> m) {

/*        OkHttpClient client = new OkHttpClient();

        MediaType header = MediaType.parse("application/vnd.kafka.json.v2+json");
        String message = "{\"records\":[{\"key\":\"\",\"value\":" + new JSONObject(m) + "}]}";
        String topic_url = ProjectConstants.liveIP2;

        RequestBody body = RequestBody.create(header, message);
        Request request = new Request.Builder()
                .url(topic_url)
                .post(body)
                .addHeader("content-type", "application/vnd.kafka.json.v2+json")
                .addHeader("cache-control", "no-cache")
                .build();

        try {
            Response response = client.newCall(request).execute();
            System.out.println("Success: " + response.body().string());
        } catch (IOException e) {
            System.out.println("Exception: ");
            L.e(ProjectConstants.devName, ProjectConstants.projectName, "Log Class", "pushToTopic", "Error => " + e.getMessage(), "N/A");
        }*/
    }
    private static void printAndSendLogs(Map<String, String> m) {
        printLog(m);
        pushToTopic(m);

    }

    // Field Auto Generations

    /**
     * Returns the current method name.
     *
     * @return the name of method from where getMethodName is executed
     *
     */
    public static String getMethodName() {
        Throwable t = new Throwable();
        StackTraceElement[] elements = t.getStackTrace();

        return elements[1].getMethodName();
    }

    /**
     * Returns the current class name.
     *
     * @return the name of class from where getMethodName is executed
     *
     */
    public static String getClassName() {
        Throwable t = new Throwable();
        StackTraceElement[] elements = t.getStackTrace();

        return elements[1].getClassName();
    }

    /**
     * Returns the current date and time.
     *
     * @return the current date and time in yyyy-MM-dd_HH:mm:ss format.
     *
     */
    private static String generateTimeStamp() {
        return new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss").format(Calendar.getInstance().getTime());
    }
}