package com.example.DarazConsumerProducer.configurations;

import com.example.DarazConsumerProducer.cassandra.CassandraUtil;
import com.example.DarazConsumerProducer.cassandra.MyCassandraTemplate;
import com.example.DarazConsumerProducer.cassandra.dao.implementations.*;
import com.example.DarazConsumerProducer.cassandra.dao.interfaces.*;
import com.example.DarazConsumerProducer.cassandra.dao.interfaces.DarazPushUpdatesDAO;
import com.example.DarazConsumerProducer.cassandra.services.implementations.*;
import com.example.DarazConsumerProducer.cassandra.services.interfaces.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


@Configuration
@Import(CassandraUtil.class)
public class MainAppConfiguration {

    @Bean
    public UsersService getUsersService() {
        return new UsersServiceImplementation();
    }

    @Bean
    public UsersDAO getUsersDAO() {
        return new UsersDAOImplementation();
    }

    @Bean
    public DarazProductsService getDarazProductsService() {
        return new DarazProductsImplementation();
    }

    @Bean
    public DarazProductsDAO getDarazProductsDAO() {
        return new DarazProductsDAOImplementation();
    }

    @Bean
    public DarazPushUpdatesService getDarazPushUpdatesService() {
        return new DarazPushUpdatesServiceImplementation();
    }

    @Bean
    public DarazPushUpdatesDAO getDarazPushUpdatesDAO() {
        return new DarazPushUpdatesDAOImplementation();
    }

    @Bean
    public DarazPushUpdates_InitialLogsDAO getDarazPushUpdates_InitialLogsDAO() {return new DarazPushUpdates_InitialLogsDAOImplementation();
    }

    @Bean
    public DarazPushUpdates_InitialLogsService getDarazPushUpdates_InitialLogsService() {
        return new DarazPushUpdates_InitialLogsServiceImplementation();
    }

    @Bean
    public MyCassandraTemplate getMyCassandraTemplate() {
        return new MyCassandraTemplate();
    }
}
